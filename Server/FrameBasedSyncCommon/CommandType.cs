﻿using System;

namespace FrameBasedSyncFramework
{
    public static class CommandType
    {
        public const byte CommandA = 1;
        public const byte CommandB = 2;
        public const byte Movement = 255;
    }
}
