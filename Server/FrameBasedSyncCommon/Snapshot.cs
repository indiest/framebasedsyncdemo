﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FrameBasedSyncFramework
{
    [Serializable]
    public class Snapshot
    {
        public int frameIndex;
        public long frameTime;
        public PlayerState myPlayerState;
        public List<SharedPlayerState> otherPlayerStates;
        public List<ObjectState> objectStates;

        public virtual void Read(BinaryReader reader)
        {
            frameIndex = reader.ReadInt32();
            frameTime = reader.ReadInt64();
            myPlayerState = new PlayerState();
            myPlayerState.Read(reader);
            int count = reader.ReadByte();
            otherPlayerStates = new List<SharedPlayerState>(count);
            for (int i = 0; i < count; i++)
            {
                var playerState = new SharedPlayerState();
                playerState.Read(reader);
                otherPlayerStates.Add(playerState);
            }
            count = reader.ReadUInt16();
            objectStates = new List<ObjectState>(count);
            for (int i = 0; i < count; i++)
            {
                var objectState = new ObjectState();
                objectState.Read(reader);
                objectStates.Add(objectState);
            }
        }

        public virtual void Write(BinaryWriter writer)
        {
            writer.Write(frameIndex);
            writer.Write(frameTime);
            myPlayerState.Write(writer);
            writer.Write((byte)otherPlayerStates.Count);
            for (int i = 0; i < otherPlayerStates.Count; i++)
                otherPlayerStates[i].WriteShared(writer);
            writer.Write((ushort)objectStates.Count);
            for (int i = 0; i < objectStates.Count; i++)
                objectStates[i].Write(writer);
        }

        public static byte[] Serialize(object arg)
        {
            var snapshot = arg as Snapshot;
            var ms = new MemoryStream();
            using (var writer = new BinaryWriter(ms))
            {
                snapshot.Write(writer);
                return ms.ToArray();
            }
        }

        public static object Deserialize(byte[] arg)
        {
            var ms = new MemoryStream(arg);
            using (var reader = new BinaryReader(ms))
            {
                var snapshot = new Snapshot();
                snapshot.Read(reader);
                return snapshot;
            }
        }
    }
}
