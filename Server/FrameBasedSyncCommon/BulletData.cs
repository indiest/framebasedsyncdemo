﻿using System;
using System.Collections.Generic;

namespace FrameBasedSyncFramework
{
    [Serializable]
    public class BulletData
    {
        public int id;
        public float radius;
        public float moveSpeed;
        public float lifetime;
        public int damage;
        public bool destoryOnHit;

        // 测试数据
        private static Dictionary<int, BulletData> _bullets = new Dictionary<int, BulletData>()
        {
            {1, new BulletData() {id = 1, radius = 1f, moveSpeed = 0, lifetime = 0.3f, damage = 20, destoryOnHit = false} },
            {2, new BulletData() {id = 2, radius = 0.2f, moveSpeed = 5, lifetime = 3f, damage = 2, destoryOnHit = true} }
        };

        public static BulletData GetData(int id)
        {
            _bullets.TryGetValue(id, out BulletData data);
            return data;
        }
    }
}
