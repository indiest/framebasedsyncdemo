﻿using System;
using System.IO;

namespace FrameBasedSyncFramework
{
    [Obsolete]
    public struct MoveInput
    {
        public float x;
        public float y;
        public float deltaTime;

        public void Read(BinaryReader reader)
        {
            x = reader.ReadSingle();
            y = reader.ReadSingle();
            deltaTime = reader.ReadSingle();
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(x);
            writer.Write(y);
            writer.Write(deltaTime);
        }

        public static byte[] Serialize(object arg)
        {
            var move = (MoveInput)arg;
            var ms = new MemoryStream();
            using (var writer = new BinaryWriter(ms))
            {
                move.Write(writer);
                return ms.ToArray();
            }
        }

        public static object Deserialize(byte[] arg)
        {
            var ms = new MemoryStream(arg);
            using (var reader = new BinaryReader(ms))
            {
                var move = new MoveInput();
                move.Read(reader);
                return move;
            }
        }

    }
}
