﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    [Serializable]
    public class ObjectState// : ICloneable
    {
        public int id;
        public Vector3 position;

        public virtual void Read(BinaryReader reader)
        {
            id = reader.ReadInt32();
            position = reader.ReadVector3();
        }

        public virtual void Write(BinaryWriter writer)
        {
            writer.Write(id);
            writer.Write(position);
        }

        public static byte[] Serialize<T>(object arg) where T : ObjectState, new()
        {
            var state = arg as T;
            var ms = new MemoryStream();
            using (var writer = new BinaryWriter(ms))
            {
                state.Write(writer);
                return ms.ToArray();
            }
        }

        public static object Deserialize<T>(byte[] arg) where T : ObjectState, new()
        {
            var ms = new MemoryStream(arg);
            using (var reader = new BinaryReader(ms))
            {
                var state = new T();
                state.Read(reader);
                return state;
            }
        }

        /*
         * FIXED: 不合理的设计！
        public virtual T Clone<T>() where T: ObjectState, new()
        {
            var clone = new T();
            clone.CopyProerties(this);
            return clone;
        }
        */
        public virtual ObjectState Clone()
        {
            var clone = (ObjectState)Activator.CreateInstance(GetType());
            clone.CopyProerties(this);
            return clone;
        }

        protected virtual void CopyProerties(ObjectState state)
        {
            id = state.id;
            position = state.position;
        }

        public override string ToString()
        {
            return string.Format("id={0}, pos={1}", id, position);
        }
    }
}
