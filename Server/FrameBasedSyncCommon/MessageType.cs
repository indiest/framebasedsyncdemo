﻿using System;

namespace FrameBasedSyncFramework
{
    public static class MessageType
    {
        public const byte CS_INPUT = 200;
        public const byte SC_SNAPSHOT = 199;
        public const byte SC_TIME = 198;
        public const byte SC_SETTINGS = 197;
        public const byte CS_OBJECT_ID = 196;
    }
}
