﻿using System;
using System.Collections.Generic;

namespace FrameBasedSyncFramework
{
    [Obsolete]
    public class InputCommandData
    {
        public byte type;
        public float cooldown;
        public BulletData bulletData;

        // 测试数据
        private static Dictionary<byte, InputCommandData> _commands = new Dictionary<byte, InputCommandData>()
        {
            {CommandType.CommandA, new InputCommandData(){type = CommandType.CommandA, cooldown = 0.5f, bulletData = BulletData.GetData(1)}},
            {CommandType.CommandB, new InputCommandData(){type = CommandType.CommandB, cooldown = 1f, bulletData = BulletData.GetData(2)}},
        };

        public static InputCommandData GetData(byte type)
        {
            InputCommandData data = null;
            _commands.TryGetValue(type, out data);
            return data;
        }
    }
}
