﻿using System;
using System.IO;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    public struct CommandInput
    {
        public byte type;
        public Vector3 lookDir;
        public Vector2 moveDir;
        // 操作指令创建的物件ID
        public int[] objIds;
        public float deltaTime;

        public void Read(BinaryReader reader)
        {
            type = reader.ReadByte();
            lookDir = reader.ReadVector3();
            moveDir = reader.ReadVector2();
            objIds = new int[reader.ReadUInt16()];
            for (int i = 0; i < objIds.Length; i++)
                objIds[i] = reader.ReadInt32();
            deltaTime = reader.ReadSingle();
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(type);
            writer.Write(lookDir);
            writer.Write(moveDir);
            if (objIds == null)
                writer.Write((ushort)0);
            else
            {
                writer.Write((ushort)objIds.Length);
                for (int i = 0; i < objIds.Length; i++)
                    writer.Write(objIds[i]);
            }
            writer.Write(deltaTime);
        }

        public static byte[] Serialize(object arg)
        {
            var command = (CommandInput)arg;
            var ms = new MemoryStream();
            using (var writer = new BinaryWriter(ms))
            {
                command.Write(writer);
                return ms.ToArray();
            }
        }

        public static object Deserialize(byte[] arg)
        {
            var ms = new MemoryStream(arg);
            using (var reader = new BinaryReader(ms))
            {
                var command = new CommandInput();
                command.Read(reader);
                return command;
            }
        }


        public static byte[] SerializeArray(object arg)
        {
            var commands = (CommandInput[])arg;
            var ms = new MemoryStream();
            using (var writer = new BinaryWriter(ms))
            {
                writer.Write((byte)commands.Length);
                foreach (var command in commands)
                {
                    command.Write(writer);
                }
                return ms.ToArray();
            }
        }

        public static object DeserializeArray(byte[] arg)
        {
            var ms = new MemoryStream(arg);
            using (var reader = new BinaryReader(ms))
            {
                var commands = new CommandInput[reader.ReadByte()];
                for (int i = 0; i < commands.Length; i++)
                {
                    commands[i].Read(reader);
                }
                return commands;
            }
        }

    }
}
