﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    [Serializable]
    public class SharedPlayerState : ObjectState
    {
        public static readonly Vector3 defaultForwardDir = Vector3.up;
        public static readonly Vector3 defaultUpDir = Vector3.back;

        public int actorNr;
        public int health = 100;
        public float moveSpeed = 3f;
        public Vector3 lookDir = defaultForwardDir;

        public CommandInput[] commands = new CommandInput[0];

        // 不会发生变化的属性，不需要序列化
        [NonSerialized]
        public float radius = 0.5f;

        public override void Read(BinaryReader reader)
        {
            base.Read(reader);
            actorNr = reader.ReadInt32();
            health = reader.ReadInt32();
            moveSpeed = reader.ReadSingle();
            lookDir = reader.ReadVector3();

            commands = new CommandInput[reader.ReadByte()];
            for (int i = 0; i < commands.Length; i++)
            {
                commands[i].Read(reader);
            }
        }

        public void WriteShared(BinaryWriter writer)
        {
            base.Write(writer);
            writer.Write(actorNr);
            writer.Write(health);
            writer.Write(moveSpeed);
            writer.Write(lookDir);


            writer.Write((byte)commands.Length);
            for (int i = 0; i < commands.Length; i++)
            {
                commands[i].Write(writer);
            }
        }

        public override void Write(BinaryWriter writer)
        {
            WriteShared(writer);
        }

        protected override void CopyProerties(ObjectState arg)
        {
            base.CopyProerties(arg);
            var state = arg as SharedPlayerState;
            actorNr = state.actorNr;
            health = state.health;
            moveSpeed = state.moveSpeed;
            lookDir = state.lookDir;
            /* 
             * 发送给客户端的消息中应该不止一个快照，而是从输入消息帧一直到最新的帧的快照集合。客户端获得该集合后更新本地（因为回滚导致的）过期的快照。使用过期快照插值位置会导致移动路径不准确。
            // 因为给客户端的快照总是最新的（FrameBasedSynchronizer.cs L222），而该快照并没有指令信息，导致客户端无法模拟其它玩家的操作。
            // 所以需要将指令一直复制（浅拷贝）到最新的快照中。
            commands = state.commands;
            */
        }
    }

    [Serializable]
    public class PlayerState : SharedPlayerState
    {
        public Dictionary<byte, float> commandCooldowns = new Dictionary<byte, float>();

        protected override void CopyProerties(ObjectState arg)
        {
            base.CopyProerties(arg);
            var state = arg as PlayerState;
            if (state.commandCooldowns.Count > 0)
                commandCooldowns = new Dictionary<byte, float>(state.commandCooldowns);
        }

        public override void Read(BinaryReader reader)
        {
            base.Read(reader);
            int count = reader.ReadByte();
            for (int i = 0; i < count; i++)
            {
                commandCooldowns[reader.ReadByte()] = reader.ReadSingle();
            }
        }

        public override void Write(BinaryWriter writer)
        {
            base.Write(writer);
            writer.Write((byte)commandCooldowns.Count);
            foreach (var pair in commandCooldowns)
            {
                writer.Write(pair.Key);
                writer.Write(pair.Value);
            }
        }
    }
}
