﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FrameBasedSyncFramework
{
    [Serializable]
    public class SkillData
    {
        public int id;
        public float cooldown;
        // 对应的输入指令（按键）
        public byte cmdType;
        // TODO: 同一按键的不同阶段。可用于多段攻击。
        public int phase;
        // 施放技能后立即产生的子弹数量
        public int startBulletCount;
        public BulletData startBulletData;
        // 一次技能施放最多可能产生的子弹数量 = startBulletCount + 动画事件CreateBullet的调用次数
        public int maxBulletCount;

        // 测试数据
        private static Dictionary<int, SkillData> _skills = new Dictionary<int, SkillData>()
        {
            {1, new SkillData(){id = 1, cooldown = 0.5f, cmdType = CommandType.CommandA, phase = 0, startBulletCount = 0, maxBulletCount = 1} },
            {2, new SkillData(){id = 2, cooldown = 0.2f, cmdType = CommandType.CommandB, phase = 0, startBulletCount = 1, startBulletData = BulletData.GetData(2), maxBulletCount = 1} },
        };

        public static SkillData GetData(int id)
        {
            SkillData data = null;
            _skills.TryGetValue(id, out data);
            return data;
        }

        public static SkillData GetData(byte cmdType, int phase = 0)
        {
            foreach (var data in _skills.Values)
            {
                if (data.cmdType == cmdType && data.phase == phase)
                    return data;
            }
            return null;
        }
    }
}
