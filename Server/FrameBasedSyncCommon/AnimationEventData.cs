﻿
using System;

namespace FrameBasedSyncFramework
{
    /*
     * 角色的动画事件信息。对应UnityEngine.AnimationEvent。
     */
    [Serializable]
    public class AnimationEventData
    {
        public float time;
        public string funcName;
        public int intParameter;
        public float floatParameter;
        public string stringParameter;
    }

    [Serializable]
    public class AnimationStateEventData
    {
        public string stateName;
        public AnimationEventData[] events;
    }

    [Serializable]
    public class AnimatorEventData
    {
        public AnimationStateEventData[] states;
    }
}
