﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra.Single;

namespace FrameBasedSyncFramework
{
    class MoveCommandProcessor : InputCommandProcessor
    {
        public void ProcessCommand(CommandInput cmd, int cmdIndex, ServerPlayerState playerState, ServerSnapshot snapshot, float timeInFrame)
        {
            playerState.lookDir = cmd.lookDir.normalized;
            /* 调用UnityEngine.DLL里非托管的方法会报错
            var rotation = Quaternion.AngleAxis(90f, PlayerState.defaultUpDir);
            var rightDir = rotation * playerState.lookDir;
            */
            var rightDir = Mathf.CrossProduct(playerState.lookDir, SharedPlayerState.defaultUpDir);
            playerState.lastMoveX = Mathf.Clamp(cmd.moveDir.x, -1f, 1f);
            playerState.lastMoveY = Mathf.Clamp(cmd.moveDir.y, -1f, 1f);
            // 如果优化移动指令的发送，则通过ServerPlayerState.Update预判实现移动
            if (!FBS.Default.CopyPlayerMoveDir)
            {
                playerState.position += (rightDir * playerState.lastMoveX + playerState.lookDir * playerState.lastMoveY) * playerState.moveSpeed * cmd.deltaTime;
                //playerState.x += playerState.moveSpeed * playerState.lastMoveX * cmd.deltaTime;
                //playerState.y += playerState.moveSpeed * playerState.lastMoveY * cmd.deltaTime;
            }
        }
    }
}
