﻿//using CondorHeroesCommon;
using ExitGames.Logging;
using Lite;
using Lite.Caching;
using Lite.Operations;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class FrameBasedSyncPeer : LitePeer
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        public FrameBasedSyncPeer(IRpcProtocol rpcProtocol, IPhotonPeer nativePeer)
            : base(rpcProtocol, nativePeer)
        {
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            if (IsFrameMessage(operationRequest))
            {
                var frameMsg = CreateFrameMessage(this, operationRequest, sendParameters);
                HandleFrameMessage(frameMsg, sendParameters);
                return;
            }

            base.OnOperationRequest(operationRequest, sendParameters);
        }

        private bool IsFrameMessage(OperationRequest operationRequest)
        {
            return operationRequest.OperationCode == MessageType.CS_INPUT ||
                operationRequest.OperationCode == MessageType.CS_OBJECT_ID;
        }

        private FrameBasedMsg CreateFrameMessage(FrameBasedSyncPeer peer, OperationRequest operationRequest, SendParameters sendParameters)
        {
            FrameBasedMsg msg = null;
            if (operationRequest.OperationCode == MessageType.CS_INPUT)
            {
                msg = new InputMsg(peer);
                msg.Read(operationRequest.Parameters);
            }
            else if (operationRequest.OperationCode == MessageType.CS_OBJECT_ID)
            {
                msg = new ObjectIdMessage(peer);
                msg.Read(operationRequest.Parameters);
            }
            return msg;
        }

        protected virtual void HandleFrameMessage(FrameBasedMsg frameMsg, SendParameters sendParameters)
        {
            if (this.RoomReference != null)
            {
                (this.RoomReference.Room as FrameBasedSyncRoom).HandleFrameMessage(frameMsg, sendParameters);
                return;
            }

            if (log.IsDebugEnabled)
            {
                log.DebugFormat("Received frame message on peer without a game: peerId={0}", this.ConnectionId);
            }
        }

        protected override RoomReference GetRoomReference(JoinRequest joinRequest)
        {
            return FrameBasedSyncRoomCache.Instance.GetRoomReference(joinRequest.GameId, this);
        }

        public Actor GetActor()
        {
            return (RoomReference.Room as FrameBasedSyncRoom).Players.GetActorByPeer(this);
        }
    }
}
