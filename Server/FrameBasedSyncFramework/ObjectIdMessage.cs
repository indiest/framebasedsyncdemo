﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class ObjectIdMessage : FrameBasedMsg
    {
        public int objId;
        // TODO：指令类型用于校验创建物件的上下文(技能）是否匹配
        public byte commandType;

        public ObjectIdMessage(FrameBasedSyncPeer peer) : base(MessageType.CS_OBJECT_ID, peer)
        {
        }

        public override void Read(Dictionary<byte, object> param)
        {
            base.Read(param);
            objId = (int)param[0];
            commandType = (byte)param[1];
        }

        public override void Write(Dictionary<byte, object> param)
        {
            base.Write(param);
            param[0] = objId;
            param[1] = commandType;
        }
    }
}
