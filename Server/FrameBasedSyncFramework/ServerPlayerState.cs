﻿using ExitGames.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    public class ServerPlayerState : PlayerState
    {
        protected static readonly ExitGames.Logging.ILogger logger = LogManager.GetCurrentClassLogger();

        public float lastMoveX;
        public float lastMoveY;
        // 即将创建的物件ID，用于在动画事件中绑定到服务器创建的物件上。
        private List<int> _objectIdsToCreate = new List<int>();

        public void ClearObjectIds()
        {
            _objectIdsToCreate.Clear();
        }

        public void AddObjectId(int objId)
        {
            _objectIdsToCreate.Add(objId);
            if (logger.IsDebugEnabled)
                logger.DebugFormat("Added id for object creation: {0}", objId);
        }

        public int TakeObjectId()
        {
            int objId = _objectIdsToCreate[0];
            _objectIdsToCreate.RemoveAt(0);
            return objId;
        }

        protected override void CopyProerties(ObjectState arg)
        {
            base.CopyProerties(arg);

            var state = arg as ServerPlayerState;

            /* 丢帧不进行预判，而是将客户端拉回。
             */
            if (FBS.Default.CopyPlayerMoveDir)
            {
                // 复制之前的速度到下一帧。如果帧消息丢失，服务器按照最后收到的消息进行移动预判。
                lastMoveX = state.lastMoveX;
                lastMoveY = state.lastMoveY;
            }

            _objectIdsToCreate.AddRange(state._objectIdsToCreate);

            _animationEventTasks.AddRange(state._animationEventTasks.ToArray());

            _skillMoveVel = state._skillMoveVel;
            _skillMoveTime = state._skillMoveTime;
        }

        public void Update(float deltaTime, ServerSnapshot snapshot)
        {
            /*
            for (int i = 0; i < commands.Length; i++)
            {
                var cmdProcessor = FrameBasedSynchronizer.GetInputCommandProcess(commands[i].type);
                if (cmdProcessor != null)
                    cmdProcessor.ProcessCommand(commands[i], i, this, snapshot);
            }
            */

            foreach (var pair in commandCooldowns.ToArray())
            {
                if (pair.Value > 0)
                {
                    commandCooldowns[pair.Key] = Math.Max(0, pair.Value - deltaTime);
                }
            }

            if (FBS.Default.CopyPlayerMoveDir)
            {
                // 移动预判（根据当前帧的移动速度）
                position += lookDir * lastMoveY * moveSpeed * deltaTime;
                //x += moveSpeed * lastMoveX * deltaTime;
                //y += moveSpeed * lastMoveY * deltaTime;
            }

            UpdateAnimationTasks(deltaTime, snapshot);

            UpdateSkillMove(deltaTime);
        }

        #region Skill Animation

        struct AnimationEventTask
        {
            public float countdown;
            public AnimationEventData eventData;
            public int skillId;
            public CommandInput cmdInput;
        }

        private List<AnimationEventTask> _animationEventTasks = new List<AnimationEventTask>();

        public void AddAnimationEvent(float countdown, AnimationEventData eventData, int skillId, CommandInput cmdInput)
        {
            var task = new AnimationEventTask() { countdown = countdown, eventData = eventData, skillId = skillId, cmdInput = cmdInput };
            for (int i = 0; i < _animationEventTasks.Count; i++)
            {
                // 按剩余时间顺序加入任务列表，已保证在同一帧中所有要触发的事件，按时间先后而不是插入顺序执行。
                // 举例：同一帧中释放了两个技能A和B，分别加入了动画事件A1(0.01s), A2(0.03s)和B1(0.02s), B2(0.04s)。
                // 在一帧中同时触发，顺序应该为：A1, B1, A2, B2。如果不按时间排序插入，执行顺序会为：A1, A2, B1, B2。
                if (countdown < _animationEventTasks[i].countdown)
                {
                    _animationEventTasks.Insert(i, task);
                    return;
                }
            }
            _animationEventTasks.Add(task);

            if (logger.IsDebugEnabled)
                logger.DebugFormat("Added animation event '{0}', skillId: {1}, cmdType: {2}, countdown: {3}", eventData.funcName, skillId, cmdInput.type, countdown);

        }

        public void UpdateAnimationTasks(float deltaTime, ServerSnapshot snapshot)
        {
            if (_animationEventTasks.Count > 0 && logger.IsDebugEnabled)
                logger.DebugFormat("Counting down {0} animation events, deltaTime: {1}, frame: {2}", _animationEventTasks.Count, deltaTime, snapshot.frameIndex);

            for (int i = 0; i < _animationEventTasks.Count; i++)
            {
                var task = _animationEventTasks[i];
                task.countdown -= deltaTime;
                if (task.countdown <= 0)
                {
                    TriggerAnimationEvent(task.eventData, task.skillId, task.cmdInput, snapshot);
                    _animationEventTasks.RemoveAt(i);
                    i--;
                }
                else
                {
                    // FIXED: AnimationEventTask是值类型，需要将其保存回列表
                    _animationEventTasks[i] = task;
                }
            }
        }

        private void TriggerAnimationEvent(AnimationEventData eventData, int skillId, CommandInput cmd, ServerSnapshot snapshot)
        {
            if (logger.IsDebugEnabled)
                logger.DebugFormat("Triggering animation event '{0}', skillId: {1}, cmdType: {2}, snapshot: {3}", eventData.funcName, skillId, cmd.type, snapshot.frameIndex);

            if (eventData.funcName == "CreateBullet")
            {
                int bulletId = eventData.intParameter;
                if (_objectIdsToCreate.Count == 0)
                {
                    logger.WarnFormat("Can't create bullet because no object id added, actorNr: {0}", actorNr);
                    return;
                }
                int objId = TakeObjectId();
                var objState = new ServerBulletState()
                {
                    id = objId,
                    position = this.position,
                    moveDir = lookDir,
                    Data = BulletData.GetData(bulletId),
                    SourceActorNr = actorNr
                };
                snapshot.objectStates.Add(objState);
            }
            else if (eventData.funcName == "StartSkillMove")
            {
                var parameters = eventData.stringParameter.Split(',');
                StartSkillMove(float.Parse(parameters[0]), float.Parse(parameters[1]));
            }
            else if (eventData.funcName == "StopSkillMove")
            {
                StopSkillMove();
            }
            
        }

        #endregion

        private float _skillMoveVel;
        private float _skillMoveTime;

        public void StartSkillMove(float distance, float duration)
        {
            _skillMoveVel = distance / duration;
            _skillMoveTime = duration;
        }

        public void StopSkillMove()
        {
            _skillMoveTime = 0;
        }

        private void UpdateSkillMove(float deltaTime)
        {
            if (_skillMoveTime > 0)
            {
                if (_skillMoveTime > deltaTime)
                {
                    position += _skillMoveVel * deltaTime * Vector3.right;
                    _skillMoveTime -= deltaTime;
                }
                else
                {
                    position += _skillMoveVel * _skillMoveTime * Vector3.right;
                    _skillMoveTime = 0;
                }
            }
        }
    }
}
