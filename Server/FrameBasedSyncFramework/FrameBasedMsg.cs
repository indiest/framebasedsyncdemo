﻿//using CondorHeroesCommon;
//using CondorHeroesServer;
//using CondorHeroesServer.Msg.MESSAGE;
using Lite;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public abstract class FrameBasedMsg// : MsgBase
    {
        public int frameIndex;
        public long sendTime;

        private byte _msgType;

        private FrameBasedSyncPeer _peer;
        public FrameBasedSyncPeer Peer
        {
            get { return _peer; }
        }

        public FrameBasedMsg(byte msgType, FrameBasedSyncPeer peer)
        {
            _msgType = msgType;
            _peer = peer;
        }

        public void SendToPeer(PeerBase target, SendParameters sendParameters)
        {
            OperationResponse response = new OperationResponse((byte)_msgType);
            var param = new Dictionary<byte, object>();
            Write(param);
            response.Parameters = param;
            target.SendOperationResponse(response, sendParameters);
            // 立即发送消息。尽管PhotonServer.config配置了PerPeerTransmitRatePeriodMilliseconds=10，如果 不调用Flush()，消息仍会延时发送。
            target.Flush();
        }

        public void SendToAll(SendParameters sendParam)
        {
            foreach (var player in (Peer.RoomReference.Room as FrameBasedSyncRoom).Players)
            {
                SendToPeer(player.Peer, sendParam);
            }
        }

        //public override MsgBase BuildMessage(Dictionary<byte, object> param, CondorHeroesPeer peer)
        //{
        //    if (_commucationType == MSG_COMMUCATION_TYPE.SEND)
        //    {

        //    }

        //    Read(param);

        //    base.BuildMessage(param, peer);
        //    return this;
        //}

        //public override void SendMessage(Dictionary<byte, object> param)
        //{
        //    if (_commucationType == MSG_COMMUCATION_TYPE.RECEIVE)
        //    {
        //    }

        //    Write(param);

        //    base.SendMessage(param);
        //}

        public virtual void Read(Dictionary<byte, object> param)
        {
            frameIndex = (int)param[255];
            sendTime = (long)param[254];
        }

        public virtual void Write(Dictionary<byte, object> param)
        {
            param.Add(255, frameIndex);
            param.Add(254, sendTime);
        }
    }
}
