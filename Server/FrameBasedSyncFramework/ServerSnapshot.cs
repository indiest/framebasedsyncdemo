﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace FrameBasedSyncFramework
{

    public class ServerSnapshot
    {
        public int frameIndex;
        public long frameTime;
        public ServerSnapshot nextSnapshot;
        public List<ServerPlayerState> playerStates;
        public List<ServerObjectState> objectStates = new List<ServerObjectState>();

        /* FIXED: 该列表的写入和读取都放到了FrameBasedSynchronizer.UpdateFrame所在线程。
        // 未处理消息列表。可能存在多线程写入和读取。
        public BlockingCollection<FrameBasedMsg> unprocessedMsgs = new BlockingCollection<FrameBasedMsg>();
        */
        public List<FrameBasedMsg> unprocessedMsgs = new List<FrameBasedMsg>();
        // 已处理过的消息。如果客户端的时间戳慢于服务器时间，则该时间之后的快照的所有消息需要重新处理（回滚）。
        public List<FrameBasedMsg> processedMsgs = new List<FrameBasedMsg>();
    }
}
