﻿//using CondorHeroesCommon;
//using CondorHeroesServer;
//using CondorHeroesServer.Msg.MESSAGE;
using Lite;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class InputMsg : FrameBasedMsg
    {
        //public MoveInput[] moves;
        public CommandInput[] commands;

        public InputMsg//()
            (FrameBasedSyncPeer peer)
            : base(MessageType.CS_INPUT, peer)
        {
            //_msgType = MSG_TYPE.CS_INPUT;
            //_commucationType = MSG_COMMUCATION_TYPE.SEND;
        }

        public override void Read(Dictionary<byte, object> param)
        {
            base.Read(param);
            //moves = (MoveInput[])param[0];
            commands = (CommandInput[])param[2];
        }

        public override void Write(Dictionary<byte, object> param)
        {
            base.Write(param);
            //param.Add(0, moves);
            param.Add(2, commands);
        }
    }
}
