﻿using ExitGames.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    class ServerBulletState : ServerObjectState
    {
        protected static readonly ExitGames.Logging.ILogger Log = LogManager.GetCurrentClassLogger();

        public BulletData Data { get; set; }
        public int SourceActorNr { get; set; }
        //public float moveX;
        //public float moveY;
        public Vector3 moveDir;
        private float _lifeTiming = 0;
        // 记录哪些玩家已经被击中过，不重复计算伤害
        private HashSet<int> _hitActorNrs = new HashSet<int>();

        protected override void CopyProerties(ObjectState arg)
        {
            base.CopyProerties(arg);
            var state = arg as ServerBulletState;
            Data = state.Data;
            SourceActorNr = state.SourceActorNr;
            moveDir = state.moveDir;
            _lifeTiming = state._lifeTiming;
            _hitActorNrs = new HashSet<int>(state._hitActorNrs);
        }

        public override bool Update(float deltaTime, ServerSnapshot snapshot)
        {
            if (_lifeTiming + deltaTime < Data.lifetime)
            {
                _lifeTiming += deltaTime;
            }
            else
            {
                deltaTime = Data.lifetime - _lifeTiming;
                _lifeTiming = Data.lifetime;
            }

            position += moveDir * Data.moveSpeed * deltaTime;

            foreach (var playerState in snapshot.playerStates)
            {
                if (playerState.actorNr == SourceActorNr)
                    continue;
                if (!Data.destoryOnHit && _hitActorNrs.Contains(playerState.actorNr))
                    continue;

                //var dx = Math.Abs(playerState.x - x);
                //var dy = Math.Abs(playerState.y - y);
                var distToPlayer = (playerState.position - position).magnitude;
                var hitDist = Data.radius + playerState.radius;

                if (Log.IsDebugEnabled)
                    Log.DebugFormat("Bullet {0} at {1}, distance: {2}, lifetime: {3}/{4}, snapshot: {5}",
                        id, position, distToPlayer, _lifeTiming, Data.lifetime, snapshot.frameIndex);

                // 简单用两个圆的半径计算碰撞
                if (distToPlayer <= hitDist)
                {
                    playerState.health -= Data.damage;

                    if (Log.IsDebugEnabled)
                        Log.DebugFormat("Bullet {0} hit Player{1}, health -> {2}", id, playerState.actorNr, playerState.health);
                    // TODO: 判断处理玩家死亡

                    _hitActorNrs.Add(playerState.actorNr);

                    // 命中后销毁
                    if (Data.destoryOnHit)
                        return false;
                }
            }

            return _lifeTiming < Data.lifetime;
        }

        public override string ToString()
        {
            return string.Format("{0}, speed={1}, moveDir={2}, _lifeTiming={3}", base.ToString(),
                Data.moveSpeed, moveDir, _lifeTiming);
        }
    }
}
