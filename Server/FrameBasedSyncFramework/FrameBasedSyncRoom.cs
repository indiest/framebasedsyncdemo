﻿//using CondorHeroesCommon;
//using CondorHeroesServer;
using ExitGames.Concurrency.Fibers;
using Lite;
using Lite.Caching;
using Photon.SocketServer;
using System;
using System.Collections.Generic;

namespace FrameBasedSyncFramework
{
    public class FrameBasedSyncRoom : LiteGame//CondorHeroesServer.CondorHeroesRoom //
    {
        private FrameBasedSynchronizer _frameBasedSynchronizer;
        private int _randomSeed;
        private Random _random;
        public Random Random
        {
            get { return _random; }
        }

        public ActorCollection Players
        {
            get { return Actors; }
        }

        public FrameBasedSyncRoom(string gameName, RoomCacheBase roomCache, int emptyRoomLiveTime = 0)
            : base(gameName, roomCache, emptyRoomLiveTime)
        {
            _frameBasedSynchronizer = new FrameBasedSynchronizer(this);
            _randomSeed = Environment.TickCount;
            _random = new Random(_randomSeed);
        }

        public void HandleFrameMessage(FrameBasedMsg msg, SendParameters sendParameters)
        {
            _frameBasedSynchronizer.HandleFrameMessage(msg);
        }

        /* See in FrameBasedSyncPeer.OnOperationRequest
         * 
        public override void EnqueueOperation(LitePeer peer, OperationRequest operationRequest, SendParameters sendParameters)
        {
            if (IsFrameMessage(operationRequest))
            {
                var frameMsg = CreateFrameMessage(peer, operationRequest, sendParameters);
                _frameBasedSynchronizer.HandleFrameMessage(frameMsg);
                return;
            }
            base.EnqueueOperation(peer, operationRequest, sendParameters);
        }

        private bool IsFrameMessage(OperationRequest operationRequest)
        {
            return operationRequest.OperationCode == (byte)MSG_TYPE.CS_INPUT;
        }

        private FrameBasedMsg CreateFrameMessage(LitePeer peer, OperationRequest operationRequest, SendParameters sendParameters)
        {
            FrameBasedMsg msg = null;
            if (operationRequest.OperationCode == (byte)MSG_TYPE.CS_INPUT)
            {
                msg = new InputMsg(peer);
                msg.Read(operationRequest.Parameters);
            }
            return msg;
        }
         */

        //protected override void HandleStartRound()
        //{
        //    base.HandleStartRound();
        //    OnStartGame();
        //    foreach (var player in Players)
        //    {
        //        player.Peer.SendOperationResponse(new OperationResponse((byte)MSG_TYPE.SC_TIME, _frameWatch.ElapsedMilliseconds), new SendParameters());
        //    }
        //}

        //protected override void HandleGameOver(int defeatUserId, WinFlag flag)
        //{
        //    base.HandleGameOver(defeatUserId, flag);
        //    OnEndGame();
        //}

        protected override Actor HandleJoinOperation(LitePeer peer, Lite.Operations.JoinRequest joinRequest, SendParameters sendParameters)
        {
            var actor = base.HandleJoinOperation(peer, joinRequest, sendParameters);
            if (actor != null)
            {
                /* Photon在客户端连接后已经自动同步服务器时间
                actor.Peer.SendOperationResponse(new OperationResponse(MessageType.SC_TIME,
                    new Dictionary<byte, object>() { { 0, _frameBasedSynchronizer.ServerTime } }), new SendParameters());
                */

                // 同步服务器设置：帧间隔和随机种子
                actor.Peer.SendOperationResponse(new OperationResponse(MessageType.SC_SETTINGS,
                    new Dictionary<byte, object>() { {0, FBS.Default.FrameInterval}, { 1, _randomSeed }, {2, FBS.Default.CopyPlayerMoveDir } }), new SendParameters());

                var state = new ServerPlayerState() { actorNr = actor.ActorNr, position = new UnityEngine.Vector3(_random.Next(-200, 200) / 100f, _random.Next(-200, 200) / 100f) };
                if (Players.Count == 1)
                {
                    OnStartGame();
                    _frameBasedSynchronizer.CreateSnapshot(_frameBasedSynchronizer.CurrentFrame, new List<ServerPlayerState>(new ServerPlayerState[] { state }), new List<ServerObjectState>());
                }
                else
                {
                    _frameBasedSynchronizer.AddPlayerState(state);
                }
            }

            return actor;
        }

        protected override void HandleLeaveOperation(LitePeer peer, Lite.Operations.LeaveRequest leaveRequest, SendParameters sendParameters)
        {
            base.HandleLeaveOperation(peer, leaveRequest, sendParameters);

            if (Players.Count == 0)
                OnEndGame();
        }

        protected virtual void OnStartGame()
        {
            _frameBasedSynchronizer.Start();
        }

        protected virtual void OnEndGame()
        {
            _frameBasedSynchronizer.Stop();
        }

        protected override void Dispose(bool dispose)
        {
            _frameBasedSynchronizer.Stop();
            base.Dispose(dispose);
        }
    }
}
