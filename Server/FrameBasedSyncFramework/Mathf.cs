﻿using System;
using MathNet.Numerics.LinearAlgebra.Single;
using MathNet.Spatial.Euclidean;
using UnityEngine;

namespace FrameBasedSyncFramework
{
    public static class Mathf
    {
        public static float Clamp(float value, float min, float max)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }

        public static Vector3 CrossProduct(Vector3 vecA, Vector3 vecB)
        {
            var result = new Vector3D(vecA.x, vecA.y, vecA.z).CrossProduct(new Vector3D(-vecB.x, -vecB.y, -vecB.z));//Unity是左手坐标系，而Math.Net默认为右手坐标系。
            return new Vector3((float)result.X, (float)result.Y, (float)result.Z);
        }
    }

    public static class MathNetExtensions
    {
        public static Vector ToVector(this Vector3 vec3)
        {
            return new DenseVector(new float[] { vec3.x, vec3.x, vec3.y });
        }
    }
}
