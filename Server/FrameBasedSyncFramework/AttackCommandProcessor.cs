﻿using ExitGames.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FrameBasedSyncFramework
{
    class AttackCommandProcessor : InputCommandProcessor
    {
        protected static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        
        private Random _random;

        public AttackCommandProcessor(Random random)
        {
            _random = random;
        }

        public void ProcessCommand(CommandInput cmd, int cmdIndex, ServerPlayerState playerState, ServerSnapshot snapshot, float timeInFrame)
        {
            var skillData = SkillData.GetData(cmd.type);
            if (skillData == null)
            {
                logger.Warn("Can't find skill data by cmdType: " + cmd.type);
                return;
            }

            if (cmd.objIds.Length < skillData.startBulletCount || cmd.objIds.Length > skillData.maxBulletCount)
            {
                logger.WarnFormat("Invalid objIds count: {0}, skillId: {1}", cmd.objIds.Length, skillData.id);
                return;
            }

            // 检查冷却时间
            float cooldown;
            if (playerState.commandCooldowns.TryGetValue(cmd.type, out cooldown) && cooldown > 0)
                return;

            playerState.ClearObjectIds();
            for (int i = 0; i < cmd.objIds.Length; i++)
            {
                if (i < skillData.startBulletCount)
                {
                    // 创建子弹
                    var objState = new ServerBulletState()
                    {
                        id = cmd.objIds[i],
                        position = playerState.position,
                        //moveX = Mathf.Clamp(cmd.dir.x, -1f, 1f),
                        //moveY = Mathf.Clamp(cmd.dir.y, -1f, 1f),
                        moveDir = playerState.lookDir,
                        Data = skillData.startBulletData,
                        SourceActorNr = playerState.actorNr
                    };
                    snapshot.objectStates.Add(objState);
                }
                else
                {
                    playerState.AddObjectId(cmd.objIds[i]);
                }
            }

            // 模拟客户端角色动画事件
            int skillId = GetSkillId(cmd.type);
            if (skillId > 0)
            {
                AnimationStateEventData stateData = null;
                if (DataManager.Events.TryGetValue("Skill" + skillId, out stateData))
                {
                    foreach (var evt in stateData.events)
                    {
                        // FIXED: 动画的倒计时需要根据帧中的按键时间偏移
                        playerState.AddAnimationEvent(evt.time + timeInFrame, evt, skillId, cmd);
                    }
                }
            }

            // 设置冷却时间
            playerState.commandCooldowns[cmd.type] = skillData.cooldown;

            if (logger.IsDebugEnabled)
                logger.DebugFormat("Starting Player{0}'s cooldown {1}", playerState.actorNr, cmd.type);
        }

        // TODO: 根据按键类型获取技能ID
        private int GetSkillId(byte cmdType)
        {
            return cmdType;
        }
    }
}
