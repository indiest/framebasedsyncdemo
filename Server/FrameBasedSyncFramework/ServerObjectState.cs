﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class ServerObjectState : ObjectState
    {
        /// <summary>
        /// 物件的更新逻辑，如子弹的移动和碰撞检测。
        /// </summary>
        /// <param name="deltaTime">距离上次更新的时间，单位为秒</param>
        /// <param name="snapshot">当前所在快照</param>
        /// <returns>是否将该物件状态复制到下一快照</returns>
        public virtual bool Update(float deltaTime, ServerSnapshot snapshot)
        {
            return true;
        }
    }
}
