﻿using Lite.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class FrameBasedSyncRoomCache : RoomCacheBase
    {
        public static readonly FrameBasedSyncRoomCache Instance = new FrameBasedSyncRoomCache();

        protected override Lite.Room CreateRoom(string roomId, params object[] args)
        {
            return new FrameBasedSyncRoom(roomId, Instance);
        }
    }
}
