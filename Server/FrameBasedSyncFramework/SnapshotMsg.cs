﻿//using CondorHeroesCommon;
//using CondorHeroesServer;
//using CondorHeroesServer.Msg.MESSAGE;
using Lite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class SnapshotMsg : FrameBasedMsg
    {
        //public PlayerState myPlayer;
        //public List<SharedPlayerState> otherPlayers = new List<SharedPlayerState>();
        //public List<ObjectState> objects = new List<ObjectState>();
        public List<Snapshot> snapshots = new List<Snapshot>();

        public SnapshotMsg(FrameBasedSyncPeer peer) : base(MessageType.SC_SNAPSHOT, peer) { }

        public override void Read(Dictionary<byte, object> param)
        {
            base.Read(param);
            //myPlayer = (PlayerState)param[0];
            //otherPlayers = (List<SharedPlayerState>)param[1];
            //objects = (List<ObjectState>)param[2];
            snapshots = (List<Snapshot>)param[0];
        }

        public override void Write(Dictionary<byte, object> param)
        {
            base.Write(param);
            //param.Add(0, myPlayer);
            //param.Add(1, otherPlayers);
            //param.Add(2, objects);
            param.Add(0, snapshots);
        }
    }
}
