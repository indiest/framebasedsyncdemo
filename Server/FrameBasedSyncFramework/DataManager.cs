﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Photon.SocketServer;

namespace FrameBasedSyncFramework
{
    public static class DataManager
    {
        public static Dictionary<string, AnimationStateEventData> Events { get; private set; }

        public static void LoadAll()
        {
            var eventDataPath = Path.Combine(ApplicationBase.Instance.BinaryPath, "assets/Player.json");
            var json = File.ReadAllText(eventDataPath);
            var eventData = JsonConvert.DeserializeObject<AnimatorEventData>(json);
            Events = eventData.states.ToDictionary(state => state.stateName);
        }
    }
}
