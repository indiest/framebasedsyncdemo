﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public interface InputCommandProcessor
    {
        void ProcessCommand(CommandInput cmd, int cmdIndex, ServerPlayerState playerState, ServerSnapshot snapshot, float timeInFrame);
    }
}
