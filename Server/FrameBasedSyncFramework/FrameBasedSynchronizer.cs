﻿using ExitGames.Concurrency.Fibers;
using ExitGames.Logging;
using Lite;
using Photon.SocketServer;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class FrameBasedSynchronizer
    {
        protected static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        private FrameBasedSyncRoom _room;
        private Stopwatch _serverWatch = new Stopwatch();
        //private PoolFiber _frameTimer = new PoolFiber();
        private MultimediaTimer _frameTimer = new MultimediaTimer();
        private Dictionary<int, ServerSnapshot> _snapshots = new Dictionary<int, ServerSnapshot>();

        private static Dictionary<byte, InputCommandProcessor> _inputCommandProcessors = new Dictionary<byte, InputCommandProcessor>();
        public static InputCommandProcessor GetInputCommandProcess(byte type)
        {
            InputCommandProcessor result;
            _inputCommandProcessors.TryGetValue(type, out result);
            return result;
        }
        static FrameBasedSynchronizer()
        {
            var cmdProcessor = new AttackCommandProcessor(new Random());//room.Random);
            _inputCommandProcessors.Add(CommandType.CommandA, cmdProcessor);
            _inputCommandProcessors.Add(CommandType.CommandB, cmdProcessor);
            _inputCommandProcessors.Add(CommandType.Movement, new MoveCommandProcessor());
        }

        private int _currentFrame;

        public int CurrentFrame
        {
            get { return _currentFrame; }
            //get { return (int)(ServerTime / FBS.Default.FrameInterval); }
        }

        public long ServerTime
        {
            // 服务器和客户端的StopWatch可能不同步
            //get { return _serverWatch.ElapsedMilliseconds; }
            // 超过25天会变成负数，需要强制转换为正整数
            get { return (uint)Environment.TickCount; }
            //get { return DateTime.Now.Ticks; }
        }

        public float FrameDuration
        {
            get { return FBS.Default.FrameInterval / 1000f; }
        }

        public FrameBasedSynchronizer(FrameBasedSyncRoom room)
        {
            _room = room;
            //_frameTimer.ScheduleOnInterval(UpdateFrame, 0, FBS.Default.FrameInterval);
            _frameTimer.Interval = (int)FBS.Default.FrameInterval;
            _frameTimer.Resolution = 1;
            _frameTimer.Elapsed += ((sender, e) => 
                {
#if DEBUG
                    try
                    {
                        UpdateFrame();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
#else
                        UpdateFrame();
#endif
                });

        }

        private ServerSnapshot GetSnapshot(int frameIndex)
        {
            ServerSnapshot snapshot = null;
            _snapshots.TryGetValue(frameIndex, out snapshot);
            return snapshot;
        }

        public ServerSnapshot CreateSnapshot(int frameIndex, List<ServerPlayerState> players, List<ServerObjectState> objects)
        {
            var snapshot = new ServerSnapshot()
            {
                frameIndex = frameIndex,
                frameTime = ServerTime,
                playerStates = players,
                objectStates = objects
            };
            _snapshots[frameIndex] = snapshot;
            return snapshot;
        }

        public void AddPlayerState(ServerPlayerState state)
        {
            var snapshot = GetSnapshot(CurrentFrame);
            if (snapshot != null)
                snapshot.playerStates.Add(state);
        }

        private BlockingCollection<FrameBasedMsg> _frameMessagesToDeliver = new BlockingCollection<FrameBasedMsg>();

        // 将收到的帧同步消息放入对应的快照中
        private void DeliverFrameMessage(FrameBasedMsg msg)
        {
            if (msg.frameIndex <= CurrentFrame - FBS.Default.SnapshotHistoryCount)
            {
                logger.InfoFormat("Out-dated frame ({0}) message received from peer {1}.", msg.frameIndex, msg.Peer);
                return;
            }
            // 客户端的帧索引可能不存在于服务器，需要向前查找
            for (int i = msg.frameIndex; i <= CurrentFrame; i++)
            {
                ServerSnapshot snapshot = GetSnapshot(i);
                if (snapshot != null)
                {
                    snapshot.unprocessedMsgs.Add(msg);
                    return;
                }
            }

            logger.WarnFormat("Can't find snapshot by frame index: {0}, current frame: {1}, time: {2}", msg.frameIndex, CurrentFrame, ServerTime);
        }

        // 先将消息放到一个线程安全的列表中
        public void HandleFrameMessage(FrameBasedMsg msg)
        {
            _frameMessagesToDeliver.Add(msg);
        }

        private void UpdateFrame()
        {
            _currentFrame = Math.Max(_currentFrame, (int)(ServerTime / FBS.Default.FrameInterval));

            if (logger.IsDebugEnabled)
                logger.DebugFormat("========== UpdateFrame {0}, time: {1} ==========", CurrentFrame, ServerTime);
            ServerSnapshot startRollbackSnapshot = null;
            int lastFrameIndex = CurrentFrame - FBS.Default.SnapshotHistoryCount + 1;
            ServerSnapshot prevSnapshot = null;
            //Dictionary<PeerBase, ServerSnapshot> peerSnapshotMapping = new Dictionary<PeerBase, ServerSnapshot>();
            //Dictionary<int, ServerPlayerState> statesToAnticipate = new Dictionary<int, ServerPlayerState>();
            // 1. 遍历历史快照
            for (int i = lastFrameIndex; i <= CurrentFrame; i++)
            {
                ServerSnapshot snapshot = GetSnapshot(i);
                if (snapshot == null)
                {
                    //Log.InfoFormat("Can't find snapshot by frame index: {0}, current frame: {1}", i, CurrentFrame);
                    continue;
                }

                //statesToAnticipate.Clear();
                //foreach (var playerState in snapshot.playerStates)
                //{
                //    statesToAnticipate[playerState.actorNr] = playerState;
                //}

                if (startRollbackSnapshot != null)
                {
                    // 1.1.回滚时，已经处理过的消息需要重新处理（在未处理消息前）
                    foreach (var msg in snapshot.processedMsgs)
                    {
                        if (ProcessFrameMsg(msg, snapshot))
                        {
                            //// 处理过消息的玩家不进行预判
                            //statesToAnticipate.Remove(msg.Peer.GetActor().ActorNr);
                        }
                    }
                }

                if (snapshot.unprocessedMsgs.Count > 0 && startRollbackSnapshot == null)
                {
                    startRollbackSnapshot = snapshot;
                    if (logger.IsDebugEnabled)
                        logger.DebugFormat("Found unprocessed message({0}) in snapshot {1}, rollback...", snapshot.unprocessedMsgs.Count, snapshot.frameIndex);
                }

                // 1.2. 处理该快照未处理的消息。只要存在未处理的消息，之后所有的快照都会被重新处理（回滚）
                foreach (var msg in snapshot.unprocessedMsgs)
                {
                    if (ProcessFrameMsg(msg, snapshot))
                    {
                        //// 记录每个客户端消息对应的快照
                        //peerSnapshotMapping[msg.Peer] = snapshot;
                        //SendSnapshotMsgToPeer(snapshot, msg.Peer);

                        //// 处理过消息的玩家不进行预判
                        //statesToAnticipate.Remove(msg.Peer.GetActor().ActorNr);
                    }
                }

                // 1.3. 将刚处理过的消息移到已处理列表中
                OnSnapshotMessageProcessed(snapshot);

                // 1.4. 更新快照状态，并将所有状态复制到下一快照。
                if (startRollbackSnapshot != null || i == CurrentFrame)
                {
                    UpdateSnapshot(snapshot, (snapshot.frameTime - prevSnapshot.frameTime) / 1000f, snapshot.playerStates);//statesToAnticipate.Values);
                    if (snapshot.nextSnapshot != null)
                    {
                        snapshot.nextSnapshot.playerStates = snapshot.playerStates.ConvertAll(CopyPlayerState);
                        /* 上面一行代码会导致nextSnapshot中玩家的所有commands会被覆盖，但是执行到下一快照时处理消息会重新设置commmands
                        for (int p = 0; p < snapshot.playerStates.Count; p++)
                        {
                            var nextPlayerState = (ServerPlayerState)snapshot.playerStates[p].Clone();
                            if (p < snapshot.nextSnapshot.playerStates.Count)
                            {
                                nextPlayerState.commands = (CommandInput[])snapshot.nextSnapshot.playerStates[p].commands.Clone();
                                snapshot.nextSnapshot.playerStates[p] = nextPlayerState;
                            }
                            else
                                snapshot.nextSnapshot.playerStates.Add(nextPlayerState);
                        }
                        */

                        snapshot.nextSnapshot.objectStates = snapshot.objectStates.ConvertAll(CopyObjectState);
                        if (logger.IsDebugEnabled)
                            logger.DebugFormat("Cloning to snapshot {0}, objects: {1}", snapshot.nextSnapshot.frameIndex, snapshot.nextSnapshot.objectStates.Count);
                    }
                }

                prevSnapshot = snapshot;
            }

            // 2. 将快照发送给客户端
            // FIXED: 回滚导致之前发给客户端的快照都不准确，需要重新发送。
            ServerSnapshot startSnapshot = startRollbackSnapshot ?? prevSnapshot;
            foreach (var actorNr in _room.Players.GetActorNumbers())
            {
                var actor = _room.Players.GetActorByNumber(actorNr);
                if (actor == null)
                    continue;
                /*
                // 该客户端存在历史快照映射，则发送历史快照
               if (!peerSnapshotMapping.TryGetValue(actor.Peer, out startSnapshot))
                   // 否则发送最新的快照
                   startSnapshot = prevSnapshot;
               */
                if (startSnapshot != null)
                {
                    SendSnapshotMsgToPeer(startSnapshot, actor.Peer as FrameBasedSyncPeer);
                    //Log.InfoFormat("Send Snapshot {0} to client {1}, time: {2}, pos: {3},{4}", snapshot.frameIndex, actor.Peer.ConnectionId, ServerTime,
                    //    snapshot.playerStates[0].x, snapshot.playerStates[0].y);
                }
            }

            // 3. 删除过期的帧。当前帧自增。
            _snapshots.Remove(lastFrameIndex);
            _currentFrame++;

            // 4. 将最新快照复制到下一帧，以供下次更新使用
            if (prevSnapshot != null)
            {
                var currentSnapshot = CreateSnapshot(CurrentFrame, prevSnapshot.playerStates.ConvertAll(CopyPlayerState),
                    prevSnapshot.objectStates.ConvertAll(CopyObjectState));
                if (logger.IsDebugEnabled)
                    logger.DebugFormat("Created new snapshot {0}, objects: {1}", currentSnapshot.frameIndex, currentSnapshot.objectStates.Count);
                prevSnapshot.nextSnapshot = currentSnapshot;
            }

            // 5. 将本帧收到的所有消息投递到对应的快照中
            // FIXED: 有时客户端发来的帧索引比当前帧大1，所以需要在创建了下一帧后再投递
            FrameBasedMsg frameMsg;
            while (_frameMessagesToDeliver.Count > 0)
            {
                if (_frameMessagesToDeliver.TryTake(out frameMsg))
                {
                    DeliverFrameMessage(frameMsg);
                }
            }
        }

        private void UpdateSnapshot(ServerSnapshot snapshot, float deltaTime, IEnumerable<ServerPlayerState> statesToAnticipate)
        {
            // 服务器端的预判会导致和客户端的误差。结果以服务器的为准。
            foreach (var state in statesToAnticipate)
            {
                //AnticipatePlayerState(state, deltaTime);
                state.Update(deltaTime, snapshot);

                if (logger.IsDebugEnabled)
                    logger.DebugFormat("Update Player{0} in snapshot {1}, delta time: {2}, last move = ({3}, {4})",
                        state.actorNr, snapshot.frameIndex, deltaTime, state.lastMoveX, state.lastMoveY);
            }

            for (int i = snapshot.objectStates.Count - 1; i >= 0; i--)
            {
                var objState = snapshot.objectStates[i];
                var oldPos = objState.position;
                if (objState.Update(deltaTime, snapshot))
                {
                    //if (logger.IsDebugEnabled)
                    //    logger.DebugFormat("Update {0}({1}) in snapshot {2}, pos: {3} => {4}",
                    //        objState.GetType().Name, objState.id, snapshot.frameIndex, oldPos, objState.position);
                }
                else //返回false表示物体应当销毁
                {
                    if (logger.IsDebugEnabled)
                        logger.DebugFormat("{0} no longer exists: {1}", objState.GetType().Name, objState.ToString());
                    snapshot.objectStates.RemoveAt(i);
                }
            }
        }

        private ServerPlayerState CopyPlayerState(ServerPlayerState state)
        {
            if (logger.IsDebugEnabled)
                logger.DebugFormat("Cloning Player{0}, health: {1}, lookDir: {2}", state.actorNr, state.health, state.lookDir);

            return (ServerPlayerState)state.Clone();
        }

        private ServerObjectState CopyObjectState(ServerObjectState state)
        {
            return (ServerObjectState)state.Clone();
        }

        private List<ServerPlayerState> MergePlayerStates(List<ServerPlayerState> target, List<ServerPlayerState> source)
        {
            if (target == null)
                target = new List<ServerPlayerState>();

            foreach (var sourceState in source)
            {
                if (!target.Exists(targetState => targetState.actorNr == sourceState.actorNr))
                {

                    target.Add(CopyPlayerState(sourceState));
                }
            }
            return target;
        }

        /*
        void AnticipatePlayerState(PlayerState playerState, float deltaTime)
        {
            // 更新指令冷却时间
            foreach (var pair in playerState.commandCooldowns.ToArray())
            {
                playerState.commandCooldowns[pair.Key] = Math.Max(0, pair.Value - deltaTime);
            }

            // 移动预判（根据上一帧的移动速度）
            if (playerState.moves.Length > 0)
            {
                playerState.x += playerState.moveSpeed * playerState.moves.Last().x * deltaTime;
                playerState.y += playerState.moveSpeed * playerState.moves.Last().y * deltaTime;
            }
        }
         */

        private bool ProcessFrameMsg(FrameBasedMsg msg, ServerSnapshot snapshot)
        {
            if (msg.Peer.RoomReference == null || msg.Peer.RoomReference.Room == null)
            {
                // 可能玩家已离开房间
                //Log.InfoFormat("Missing Room/Reference for peer: {0}", msg.Peer);
                return false;
            }
            //var playerState = System.Array.Find(snapshot.playerStates, state => msg.Peer.userInfo.UserId == state.userId);
            var player = msg.Peer.GetActor();
            if (player == null)
            {
                if (logger.IsInfoEnabled)
                    logger.InfoFormat("Can't find player by peer: {0}", msg.Peer);
                return false;
            }
            var playerState = snapshot.playerStates.Find(state => state.actorNr == player.ActorNr);
            if (playerState == null)
            {
                if (logger.IsInfoEnabled)
                    logger.InfoFormat("Can't find player state by actorNr: {0}", player.ActorNr);
                return false;
            }

            if (msg is InputMsg)
            {
                //playerState.moves = msg.moves;
                playerState.commands = (CommandInput[])(msg as InputMsg).commands.Clone(); //(msg as InputMsg).commands;//

                //foreach (var move in playerState.moves)
                //{
                //    playerState.lastMoveX = Mathf.Clamp(move.x, -1f, 1f);
                //    playerState.lastMoveY = Mathf.Clamp(move.y, -1f, 1f);
                //    playerState.x += playerState.moveSpeed * playerState.lastMoveX * move.deltaTime;//FrameDuration;
                //    playerState.y += playerState.moveSpeed * playerState.lastMoveY * move.deltaTime;//FrameDuration;
                //}

                /* 客户端的指令存在时间依赖性，放到ServerPlayerState.Update中处理
                 */
                float timeInFrame = 0;
                for (int i = 0; i < playerState.commands.Length; i++)
                {
                    timeInFrame += playerState.commands[i].deltaTime;
                    InputCommandProcessor cmdProcessor;
                    if (_inputCommandProcessors.TryGetValue(playerState.commands[i].type, out cmdProcessor))
                        cmdProcessor.ProcessCommand(playerState.commands[i], i, playerState, snapshot, timeInFrame);
                }

                if (logger.IsDebugEnabled)
                    logger.DebugFormat("Processed InputMessage, actor: {0}, moves: {1}, cmds: {2}, sent frame: {3}, snapshot: {4}", player.ActorNr,
                    /*playerState.moves.Length*/'-', playerState.commands.Length, msg.frameIndex, snapshot.frameIndex);

            }
            else if (msg is ObjectIdMessage)
            {
                var objIdMsg = msg as ObjectIdMessage;
                var actorNr = objIdMsg.objId >> 16;
                if (actorNr == playerState.actorNr)
                    playerState.AddObjectId(objIdMsg.objId);
                else
                    logger.WarnFormat("Invalid ObjectIdMessage, actorNr: {0}, expected: {1}", actorNr, playerState.actorNr);
            }
            else
            {
                logger.Warn("Unsupported frame message type: " + msg.GetType().Name);
                return false;
            }
            return true;
        }

        private void OnSnapshotMessageProcessed(ServerSnapshot snapshot)
        {
            snapshot.processedMsgs.AddRange(snapshot.unprocessedMsgs);
            snapshot.unprocessedMsgs.Clear();
            /*
            FrameBasedMsg msg;
            while (snapshot.unprocessedMsgs.Count > 0)
            {
                if (snapshot.unprocessedMsgs.TryTake(out msg))
                {
                    // 将未处理消息移动到已处理列表
                    snapshot.processedMsgs.Add(msg);
                    // 此时不直接发送快照给客户端
                    //SendSnapshotMsgToPeer(snapshot, msg.Peer);
                }
            }
            */
        }

        private void SendSnapshotMsgToPeer(ServerSnapshot startSnapshot, FrameBasedSyncPeer peer)
        {
            if (peer.RoomReference == null || peer.RoomReference.Room == null)
            {
                // 可能玩家已离开房间
                return;
            }
            var msg = new SnapshotMsg(peer);
            msg.frameIndex = CurrentFrame;
            msg.sendTime = ServerTime;
            var player = peer.GetActor();
            for (int i = startSnapshot.frameIndex; i <= _currentFrame; i++)
            {
                var serverSnapshot = GetSnapshot(i);
                if (serverSnapshot == null)
                    continue;
                var clientSnapshot = new Snapshot()
                {
                    frameIndex = serverSnapshot.frameIndex,
                    frameTime = serverSnapshot.frameTime,
                    otherPlayerStates = new List<SharedPlayerState>()
                };
                foreach (var state in serverSnapshot.playerStates)
                {
                    if (state.actorNr == player.ActorNr)
                    {
                        // FIXED: 此处不用发送克隆的对象
                        clientSnapshot.myPlayerState = state;
                    }
                    else
                    {
                        clientSnapshot.otherPlayerStates.Add(state);
                    }
                }
                //logger.DebugFormat("Sending snapshot {0} to player {1}, other player skill commands: {2} / {3}", clientSnapshot.frameIndex, player.ActorNr, clientSnapshot.otherPlayerStates[0].commands.Count(cmd => cmd.type != CommandType.Movement), clientSnapshot.otherPlayerStates[0].commands.Length);
                clientSnapshot.objectStates = serverSnapshot.objectStates.ConvertAll(state => new ObjectState() { id = state.id, position = state.position });

                msg.snapshots.Add(clientSnapshot);
            }
            msg.SendToPeer(msg.Peer, new SendParameters() { Unreliable = FBS.Default.SnapshotMsgUnreliable });
        }

        public void Start()
        {
            _serverWatch.Restart();
            _frameTimer.Start();
            _currentFrame = (int)(ServerTime / FBS.Default.FrameInterval);
        }

        public void Stop()
        {
            _serverWatch.Stop();
            _frameTimer.Stop();
            _snapshots.Clear();
            _currentFrame = 0;
        }
    }
}
