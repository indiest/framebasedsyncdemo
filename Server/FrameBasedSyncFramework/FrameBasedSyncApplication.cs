﻿//using CondorHeroesServer;
using Lite;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameBasedSyncFramework
{
    public class FrameBasedSyncApplication : LiteApplication//CondorHeroesAplication
    {

        static FrameBasedSyncApplication()
        {
            //Protocol.AllowRawCustomValues = true;
            Protocol.TryRegisterCustomType(typeof(PlayerState), 255, ObjectState.Serialize<PlayerState>, ObjectState.Deserialize<PlayerState>);
            Protocol.TryRegisterCustomType(typeof(SharedPlayerState), 254, ObjectState.Serialize<SharedPlayerState>, ObjectState.Deserialize<SharedPlayerState>);
            //Protocol.TryRegisterCustomType(typeof(MoveInput), 253, MoveInput.Serialize, MoveInput.Deserialize);
            Protocol.TryRegisterCustomType(typeof(ObjectState), 252, ObjectState.Serialize<ObjectState>, ObjectState.Deserialize<ObjectState>);
            Protocol.TryRegisterCustomType(typeof(CommandInput), 251, CommandInput.Serialize, CommandInput.Deserialize);
            //Protocol.TryRegisterCustomType(typeof(CommandInput[]), 250, CommandInput.SerializeArray, CommandInput.DeserializeArray);
            Protocol.TryRegisterCustomType(typeof(Snapshot), 249, Snapshot.Serialize, Snapshot.Deserialize);
        }

        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            return new FrameBasedSyncPeer(initRequest.Protocol, initRequest.PhotonPeer);
        }

        protected override void Setup()
        {
            base.Setup();

            DataManager.LoadAll();

#if DEBUG
            System.Diagnostics.Debugger.Launch();
#endif
        }

        protected override void OnStopRequested()
        {
            base.OnStopRequested();
            // 应用重新加载后，移出所有房间
            var roomNames = FrameBasedSyncRoomCache.Instance.GetRoomNames();
            foreach (var roomName in roomNames)
            {
                Room room;
                if (FrameBasedSyncRoomCache.Instance.TryGetRoomWithoutReference(roomName, out room))
                {
                    FrameBasedSyncRoomCache.Instance.TryRemoveRoomInstance(room);
                }
            }
        }
    }
}
