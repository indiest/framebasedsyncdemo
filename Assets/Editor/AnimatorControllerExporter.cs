﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FrameBasedSyncFramework;

public class AnimatorControllerExporter : ScriptableObject
{
    private static readonly string[] availableEvents = new string[]
    {
        "CreateBullet", "StartSkillMove", "StopSkillMove"
    };

    public static AnimatorEventData ToAnimatorEventData(AnimatorController ac)
    {
        var states = new List<AnimationStateEventData>();
        foreach (var layer in ac.layers)
        {
            foreach (var state in layer.stateMachine.states)
            {
                if (state.state.motion != null)
                {
                    var clip = state.state.motion as AnimationClip;
                    if (clip.events == null || clip.events.Length == 0)
                        continue;
                    var stateData = new AnimationStateEventData()
                    {
                        stateName = state.state.name
                    };
                    var events = new List<AnimationEventData>();
                    foreach (var evt in clip.events)
                    {
                        if (availableEvents.Contains(evt.functionName))
                        {
                            events.Add(new AnimationEventData()
                            {
                                time = evt.time,
                                funcName = evt.functionName,
                                intParameter = evt.intParameter,
                                floatParameter = evt.floatParameter,
                                stringParameter = evt.stringParameter
                            });
                        }
                    }
                    stateData.events = events.ToArray();
                    states.Add(stateData);
                }
            }
        }

        return new AnimatorEventData() { states = states.ToArray() };
    }

    [MenuItem("Assets/Export Animator Events...", true)]
    static bool ValidateExportEvents()
    {
        return Selection.activeObject is AnimatorController;
    }

    [MenuItem("Assets/Export Animator Events...", false, 103)]
    static void ExportEvents()
    {
        var ac = Selection.activeObject as AnimatorController;
        var stateEvents = ToAnimatorEventData(ac);

        var path = EditorUtility.SaveFilePanel("Save JSON to", "", ac.name, "json");
        var json = JsonUtility.ToJson(stateEvents, true);
        File.WriteAllText(path, json);

    }
}