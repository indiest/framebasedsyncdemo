﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using FrameBasedSyncFramework;
using DG.Tweening;

public class Player : ClientObject
{
    public SharedPlayerState playerState { get { return state as SharedPlayerState; } }

    public TextMesh healthText;
    public TextMesh damageTextPrefab;
    public Animator animator;

    private DemoClient _client;

    // 即将创建的物件ID，用于在动画事件中绑定到服务器创建的物件上。
    private List<int> _objectIdsToCreate = new List<int>();

    public int[] GetObjectIds() { return _objectIdsToCreate.ToArray(); }

    public void ClearObjectIds()
    {
        _objectIdsToCreate.Clear();
    }

    public void AddObjectId(int objId)
    {
        _objectIdsToCreate.Add(objId);
    }

    public int TakeObjectId()
    {
        int objId = _objectIdsToCreate[0];
        _objectIdsToCreate.RemoveAt(0);
        return objId;
    }

    public void SetClient(DemoClient client)
    {
        _client = client;
    }

    void Start()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        healthText.gameObject.layer = gameObject.layer;
    }

    public void TakeDamage(int value)
    {
        var damageText = Util.Instantiate(damageTextPrefab, transform.parent);
        damageText.transform.position = transform.position + Vector3.up * 0.5f;
        damageText.text = "-" + value;
        damageText.gameObject.layer = gameObject.layer;
        damageText.gameObject.SetActive(true);
        damageText.transform.DOMoveY(0.5f, 1f).SetRelative().OnComplete(() => Destroy(damageText.gameObject));
    }

    void Update()
    {
        if (state != null)
            healthText.text = playerState.health.ToString();
        healthText.transform.LookAt(_client.clientCamera.transform);//.forward = Vector3.forward;
    }

    public override void LerpTo(float duration, float maxSpeed = float.MaxValue)
    {
        base.LerpTo(duration, maxSpeed);

        if (state != null)
        {
            transform.forward = playerState.lookDir;
        }
    }

    // 动画回调函数
    private void CreateBullet(int bulletId)
    {
        /* 有了ObjectIds，非主控玩家也可以通过动画创建子弹了！
        // 只有客户端控制的玩家才在动画中直接创建子弹，其他玩家的子弹在更新快照时创建
        if (this != _client.MyPlayer)
            return;
        */

        var commandHandler = _client.commandHandlers.FirstOrDefault(h => h.bulletPrefab != null && h.bulletPrefab.Data.id == bulletId);
        if (commandHandler == null)
        {
            Debug.LogError("Can't find command handler by bulletId: " + bulletId);
            return;
        }
        var bullet = commandHandler.CreateBullet(this);
        bullet.transform.forward = transform.forward;

        bullet.id = TakeObjectId();
        _client.RegisterObject(bullet);

        /*
        // 向服务器同步对象ID
        _client.Peer.OpCustom(MessageType.CS_OBJECT_ID, new Dictionary<byte, object>()
        {
            {0, objId }, {1, (byte)commandHandler.type }, {254, _client.ServerTime }, {255, _client.ServerFrameIndex }
        }, _client.sendReliable);
        */
    }


}
