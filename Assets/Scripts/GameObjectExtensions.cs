﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static void SetChildrenLayer(this GameObject go, int layer)
    {
        go.layer = layer;
        for (int i = 0; i < go.transform.childCount; i++)
        {
            SetChildrenLayer(go.transform.GetChild(i).gameObject, layer);
        }
    }
}