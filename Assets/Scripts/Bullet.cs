﻿using UnityEngine;
using System.Collections;
using FrameBasedSyncFramework;

public class Bullet : ClientObject
{
    public int dataId;

    public BulletData Data
    {
        get { return BulletData.GetData(dataId); }
    }

    //public Vector3 moveDir;

    private float _lifeTiming;

    void Start()
    {
        var collider = GetComponent<SphereCollider>();
        if (collider != null)
            collider.radius = Data.radius;
    }

    void Update()
    {
        float deltaTime = Time.unscaledDeltaTime;
        if (_lifeTiming + deltaTime < Data.lifetime)
        {
            _lifeTiming += deltaTime;
        }
        else
        {
            deltaTime = Data.lifetime - _lifeTiming;
            _lifeTiming = Data.lifetime;

            // 物件的到期销毁是否由服务器控制?
            if (clientAuthority)
                Destroy(gameObject);
        }
        /*
        float angle = Vector2.Angle(Vector2.right, moveDir);
        if (moveDir.y < 0) angle = -angle;
        transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.position += moveDir * Data.moveSpeed * Time.unscaledDeltaTime;
         */
        transform.position += transform.forward * Data.moveSpeed * deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponentInParent<Player>();
        if (player != source)
        {
            if (player != null)
                player.TakeDamage(Data.damage);

            // 物件的碰撞销毁是否由服务器控制?
            if (clientAuthority && Data.destoryOnHit)
                Destroy(gameObject);
        }
    }
}
