﻿using UnityEngine;
using UnityEngine.UI;

public class MobileStartupWindow : MonoBehaviour
{

    public InputField inputIP;
    public InputField inputPort;
    public Button buttonStart;
    public DemoClient[] demoClients;
    public Toggle[] toggleClients;

    void Awake()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        foreach (var client in demoClients)
        {
            client.gameObject.SetActive(false);
        }
        buttonStart.onClick.AddListener(OnStart);
#else
        gameObject.SetActive(false);
#endif

    }

    private void OnStart()
    {
        string serverAddress = inputIP.text + ":" + inputPort.text;
        //foreach (var client in demoClients)
        for (int i = 0; i < demoClients.Length; i++)
        {
            var client = demoClients[i];
            client.serverAddress = serverAddress;
            client.gameObject.SetActive(toggleClients.Length > i ? toggleClients[i].isOn : false);
        }
        //demoClients[0].enableSimulation = toggleNS1.isOn;
        //demoClients[0].simIncomingLag = demoClients[0].simOutgoingLag = (int)sliderNSLag1.value;//int.Parse(sliderNSLag1.text);
        //demoClients[0].simIncomingLoss = demoClients[0].simOutgoingLoss = (int)sliderNSLoss1.value;//int.Parse(sliderNSLoss1.text);
        //demoClients[1].enableSimulation = toggleNS2.isOn;
        //demoClients[1].simIncomingLag = demoClients[1].simOutgoingLag = (int)sliderNSLag2.value;//int.Parse(sliderNSLag2.text);
        //demoClients[1].simIncomingLoss = demoClients[1].simOutgoingLoss = (int)sliderNSLoss2.value;//int.Parse(sliderNSLoss2.text);
        gameObject.SetActive(false);
    }
}
