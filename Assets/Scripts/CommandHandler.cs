﻿using UnityEngine;
using FrameBasedSyncFramework;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CommandHandler : MonoBehaviour
{
    public byte type;
    public string inputButton;
    public string animationName;
    //public bool createBulletInAnimation;
    public Bullet bulletPrefab;
    public CommandButton uiButton;
    public Text cooldownText;

    public float cooldown;
    public float serverCooldown;
    private DemoClient _client;

    public void SetClient(DemoClient client)
    {
        _client = client;
    }

    public bool CheckInput(ref CommandInput cmd)
    {
#if UNITY_ANDROID || UNITY_IPHONE
        if (!uiButton.IsPressed)
            return false;
#else
        if (!Input.GetButton(inputButton))
            return false;
#endif

        if (cooldown > 0 || serverCooldown > 0)
            return false;

        cmd.type = type;
        var bias = Quaternion.AngleAxis(Random.Range(-15f, 15f), SharedPlayerState.defaultUpDir);
        cmd.lookDir = bias * _client.MyPlayer.transform.forward;
        cmd.deltaTime = Time.unscaledDeltaTime;

        var skillData = SkillData.GetData(type);

        cooldown = skillData.cooldown;

        _client.MyPlayer.ClearObjectIds();
        for (int i = 0; i < skillData.maxBulletCount; i++)
        {
            _client.MyPlayer.AddObjectId(_client.GetRandomObjectId());
        }
        cmd.objIds = _client.MyPlayer.GetObjectIds();

        if (bulletPrefab.clientAuthority)
        {
            for (int i = 0; i < skillData.startBulletCount; i++)
            {
                var bullet = CreateBullet(_client.MyPlayer);
                bullet.transform.forward = cmd.lookDir;
                bullet.id = _client.MyPlayer.TakeObjectId();
                _client.RegisterObject(bullet);
            }
        }

        if (!string.IsNullOrEmpty(animationName))
        {
            _client.MyPlayer.animator.Play(animationName);
        }

        return true;
    }

    // 根据预制件创建子弹物件。注意：返回的物件没有设置ID。
    public Bullet CreateBullet(Player source)
    {
        var bullet = Util.Instantiate(bulletPrefab, _client.transform);
        //bullet.Data = InputCommandData.GetData(type).bulletData;
        bullet.gameObject.SetChildrenLayer(_client.gameObject.layer);
        bullet.source = source;
        bullet.transform.position = source.transform.position;
        bullet.gameObject.SetActive(true);
        return bullet;
    }

    void Update()
    {
        if (cooldown > 0)
            cooldown = Mathf.Max(0, cooldown - Time.unscaledDeltaTime);

        if (cooldownText != null)
            cooldownText.text = string.Format("{0:0.0}\n({1:0.0})", cooldown, serverCooldown);

        uiButton.bg.color = (cooldown > 0 || serverCooldown > 0) ? Color.gray : Color.white;
    }
}
