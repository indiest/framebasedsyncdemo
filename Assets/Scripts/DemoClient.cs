﻿using ExitGames.Client.Photon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lite.Operations;
using FrameBasedSyncFramework;
using System.Text;
using System.Linq;

public class DemoClient : MonoBehaviour, IPhotonPeerListener
{
    public string serverAddress = "127.0.0.1:5055";
    public string applicationName = "Default";
    public string roomName;
    public Text logText;
    public Text timeText;
    public Player playerPrefab;
    public Color[] playerColors = new Color[] { Color.white, Color.green, Color.yellow };
    public float moveSpeed = 3f;
    public float rotateSpeed = 360f;
    public string inputAxisX = "Horizontal";
    public string inputAxisY = "Vertical";
    public string inputAxisZ = "Rotate";
    public bool mouseControl;
    public VirtualDPad virtualDPad;
    public Camera clientCamera;

    public CommandHandler[] commandHandlers;

    [System.NonSerialized]
    public int frameIntervalInMs = 50;
    public int clientOffsetTimeInMs = 200;
    public bool sendReliable = false;
    [System.NonSerialized]
    public bool optimizedMoveCommands = false;

    [Header("Network Simulation")]
    public bool enableSimulation = false;
    public int simOutgoingLag = 0;
    [Range(0, 100)]
    public int simOutgoingLoss = 0;
    public int simIncomingLag = 0;
    [Range(0, 100)]
    public int simIncomingLoss = 0;

    [Header("Debug Settings")]
    public bool drawFramesAhead = false;

    private PhotonPeer _peer;
    public PhotonPeer Peer { get { return _peer; } }

    private int _joinServerTime;
    private System.Random _random;
    //private long _serverTimeOffset;
    //private System.Diagnostics.Stopwatch _frameWatch = new System.Diagnostics.Stopwatch();
    public long ServerTime
    {
        //get { return _serverTimeOffset + _frameWatch.ElapsedMilliseconds; }
        // 服务器时间取自Environment.TickCount，可能为负值
        get { return (uint)_peer.ServerTimeInMilliSeconds; }
    }
    public int ServerFrameIndex
    {
        get { return (int)(ServerTime / frameIntervalInMs); }
    }
    public long ClientTime
    {
        get { return ServerTime - clientOffsetTimeInMs; }
    }
    public int ClientFrameIndex
    {
        get { return (int)(ClientTime / frameIntervalInMs); }
    }

    public float FrameDeltaTime
    {
        get
        {
#if UNITY_5
            return Time.fixedUnscaledDeltaTime;
#else
            return Time.fixedDeltaTime / Time.timeScale;
#endif
        }
        //get { return frameIntervalInMs / 1000f; }
    }

    public Player MyPlayer { get; private set; }
    private int _myActorNr;
    private Dictionary<int, Player> _otherPlayers = new Dictionary<int, Player>();
    public Dictionary<int, Player> OtherPlayers { get { return _otherPlayers; } }
    private Dictionary<int, ClientObject> _objects = new Dictionary<int, ClientObject>();
    private Dictionary<int, Snapshot> _snapshots = new Dictionary<int, Snapshot>();
    private Snapshot _lastProcessedSnapshot;
    private int _maxReceivedFrame = -1;
	// 记录停止自己控制的角色停止移动的时间点，用于对其进行位置插值（移动时不插值）
	private int _stopMovingServerFrameIndex = int.MaxValue;

    static DemoClient()
    {
        Protocol.TryRegisterType(typeof(PlayerState), 255, ObjectState.Serialize<PlayerState>, ObjectState.Deserialize<PlayerState>);
        Protocol.TryRegisterType(typeof(SharedPlayerState), 254, ObjectState.Serialize<SharedPlayerState>, ObjectState.Deserialize<SharedPlayerState>);
        //Protocol.TryRegisterType(typeof(MoveInput), 253, MoveInput.Serialize, MoveInput.Deserialize);
        Protocol.TryRegisterType(typeof(ObjectState), 252, ObjectState.Serialize<ObjectState>, ObjectState.Deserialize<ObjectState>);
        Protocol.TryRegisterType(typeof(CommandInput), 251, CommandInput.Serialize, CommandInput.Deserialize);
        //Protocol.TryRegisterType(typeof(CommandInput[]), 250, CommandInput.SerializeArray, CommandInput.DeserializeArray);
        Protocol.TryRegisterType(typeof(Snapshot), 249, Snapshot.Serialize, Snapshot.Deserialize);

    }

    void Awake()
    {
        Application.runInBackground = true;
#if UNITY_ANDROID || UNITY_IPHONE
        Application.targetFrameRate = 30;
#else
        Application.targetFrameRate = 60;
#endif

        _peer = new PhotonPeer(this, ConnectionProtocol.Udp);

        _peer.TrafficStatsEnabled = true;

        _peer.IsSimulationEnabled = enableSimulation;
        _peer.NetworkSimulationSettings.OutgoingLag = simOutgoingLag;
        _peer.NetworkSimulationSettings.OutgoingLossPercentage = simOutgoingLoss;
        _peer.NetworkSimulationSettings.IncomingLag = simIncomingLag;
        _peer.NetworkSimulationSettings.IncomingLossPercentage = simIncomingLoss;

        if (clientCamera == null)
            clientCamera = GetComponentInChildren<Camera>();
    }

	void Start()
    {
        _peer.Connect(serverAddress, applicationName);

        if (string.IsNullOrEmpty(roomName))
            roomName = System.Guid.NewGuid().ToString();

        if (commandHandlers == null || commandHandlers.Length == 0)
            commandHandlers = GetComponents<CommandHandler>();
        foreach (var handler in commandHandlers)
        {
            handler.SetClient(this);
        }

        _lastMousePos = Input.mousePosition;
    }

    public int GetRandomObjectId()
    {
        // 物件ID由ActorNr和16位随机数组成，保证每个玩家生成的不相同
        return MyPlayer.playerState.actorNr << 16 | _random.Next(0, ushort.MaxValue);
    }

    public void RegisterObject(ClientObject obj)
    {
        if (_objects.ContainsKey(obj.id))
            Debug.LogWarning("Registering object with existing id: " + obj.id);
        _objects[obj.id] = obj;
    }

    private string GetSnapshotDebugString()
    {
        var sb = new StringBuilder("Snapshots: [");
        foreach (var key in _snapshots.Keys)
        {
            sb.Append(key);
            sb.Append(", ");
        }
        sb.Append(']');
        return sb.ToString();
    }

    private bool CheckGameStarted()
    {
        if (MyPlayer == null)
            return false;

        // 客户端进入房间/开始游戏后等待200毫秒再开始帧逻辑（渲染时间比收到最新消息要晚）
        if (ServerTime - _joinServerTime <= clientOffsetTimeInMs)
            return false;

        if (_snapshots.Count == 0)
            return false;

        return true;
    }

    private int _receivedSnapshotsCount = 0;
    private int _startFrameIndex = 0;
    private float GetIncomingMessageLossRate()
    {
        //return (float)(Peer.PacketLossByChallenge + Peer.PacketLossByCrc) / Peer.TrafficStatsIncoming.TotalPacketCount;
        // 按照快照收到总比例计算丢包率
        return 1f - (float)_receivedSnapshotsCount / (ServerFrameIndex - _startFrameIndex);
    }

    // 调用频率由TimeManager.fixedTimestep决定
    void FixedUpdate()
    {
        timeText.text = string.Format("Client: {0}@{1}\nServer: {2}@{3}\nLatest frame: {4}\nPing: {5}ms, Loss(in): {6:0.0}%",
            ClientTime, ClientFrameIndex, ServerTime, ServerFrameIndex, _maxReceivedFrame, Peer.RoundTripTime, 100f * GetIncomingMessageLossRate());// GetSnapshotDebugString());

        if (_peer != null)
        {
            _peer.Service();

            if (!CheckGameStarted())
                return;

            //UpdateSnapshot();
            var snapshot = FindNextSnapshot();
            // 不重复处理快照(但是会继续插值)
            if (snapshot == _processingSnapshot)
                return;
            _processingSnapshot = snapshot;
            if (_processingSnapshot != null)
            {
                //Debug.LogFormat("{0}(actorNr={1}) processing snapshot {2}, other player commands: {3}",
                //    name, _myActorNr, _processingSnapshot.frameIndex, _processingSnapshot.otherPlayerStates[0].commands.Length);
                foreach (var state in _processingSnapshot.otherPlayerStates)
                {
                    var otherPlayer = _otherPlayers[state.actorNr];
                    // 如果快照中存在其他玩家的指令，则执行
                    foreach (var cmd in state.commands)
                    {
                        if (cmd.type == CommandType.Movement)
                            continue;

                        var handler = commandHandlers.First(h => h.type == cmd.type);
                        if (handler == null)
                        {
                            DebugReturn(DebugLevel.ERROR, "Can't find handler by type: " + cmd.type);
                            continue;
                        }

                        // 如果物件（子弹）由服务器控制，则在状态插值时才创建。
                        if (handler.bulletPrefab.clientAuthority)
                        {
                            var skillData = SkillData.GetData(cmd.type);
                            for (int i = 0; i < cmd.objIds.Length; i++)
                            {
                                if (i < skillData.startBulletCount)
                                    CreateBullet(cmd.objIds[i], otherPlayer, cmd, handler);
                                else
                                    otherPlayer.AddObjectId(cmd.objIds[i]);
                            }
                        }

                        // 播放指令相关动画，如攻击动作
                        if (!string.IsNullOrEmpty(handler.animationName))
                        {
                            otherPlayer.animator.Play(handler.animationName);
                        }
                    }
                }

                _lastProcessedSnapshot = _processingSnapshot;
            }

            RemoveOutdatedSnapshots();

            if (_commands.Count == 0)
                return;

            _peer.OpCustom(MessageType.CS_INPUT, new Dictionary<byte, object>() {
                /*{0, _moves.ToArray()}, */{2, _commands.ToArray()},
                // UNDONE: 客户端本地对象（主角及其创建的子弹等）总是预先开始行动，所以时间轴是和服务器重合的，超前于客户端（其它非本地对象)的时间轴。
                {254, ServerTime}, {255, ServerFrameIndex}}, sendReliable);
            _peer.SendOutgoingCommands();

            //Debug.LogFormat("{0}(actorNr={1}) sent {2} commands in frame {3}", name, _myActorNr, _commands.Count, ServerFrameIndex);

            _commands.Clear();
        }
    }

    private List<CommandInput> _commands = new List<CommandInput>();
    private float _lastMoveX = 0;
    private float _lastMoveY = 0;
    private float _lastRotateZ = 0;
    private Vector2 _lastMousePos;

    void Update()
    {
        if (!CheckGameStarted())
            return;

        // 输入采样
#if UNITY_ANDROID || UNITY_IPHONE
        var moveDir = virtualDPad.Direction;
        var moveX = moveDir.x;
        var moveY = moveDir.y;
        var moveZ = 0;
#else
        // A/D键：左右平移
        var moveX = Input.GetAxis(inputAxisX);
        // W/S键：前后移动
        var moveY = Input.GetAxis(inputAxisY);
        // Q/E键：顺时针/逆时针转动
        var moveZ = Input.GetAxis(inputAxisZ);
#endif
        bool mouseMoved = false;
        if (mouseControl)
            mouseMoved = !Mathf.Approximately(Input.mousePosition.x, _lastMousePos.x) || !Mathf.Approximately(Input.mousePosition.y, _lastMousePos.y);
        /*
        if (optimizedMoveCommands && Mathf.Approximately(moveX, _lastMoveX) && Mathf.Approximately(moveY, _lastMoveY))
        {
            // 优化流量：当移动方向没有改变时不发生移动指令
            //DebugReturn(DebugLevel.OFF, string.Format("Optimizing move commands ({0}, {1})", moveX, moveY));
        }
        else
        */
        {
            if (mouseMoved)
            {
                var mousePos = clientCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                    clientCamera.WorldToScreenPoint(Vector3.zero).z));
                //mousePos.z = 0;
                //mousePos.Normalize();
                //DebugReturn(DebugLevel.OFF, string.Format("Mouse moved {0} -> {1}, in world: {2}", _lastMousePos, (Vector2)Input.mousePosition, mousePos));
                MyPlayer.transform.LookAt(mousePos);//, SharedPlayerState.defaultUpDir);
            }
            else
            {
                MyPlayer.transform.Rotate(SharedPlayerState.defaultUpDir, moveZ * rotateSpeed * Time.unscaledDeltaTime, Space.World);
            }

            var move = new CommandInput()
            {
                type = CommandType.Movement,
                lookDir = MyPlayer.transform.forward,
                moveDir = new Vector2(moveX, moveY),
                deltaTime = Time.unscaledDeltaTime
            };
            _commands.Add(move);
        }

        // 此处不能用MyPlayer.transform.right
        var rightDir = Vector3.Cross(MyPlayer.transform.forward, -SharedPlayerState.defaultUpDir);
        MyPlayer.transform.position += (rightDir * moveX + MyPlayer.transform.forward * moveY) * moveSpeed * Time.unscaledDeltaTime;
        //MyPlayer.transform.position += new Vector3(moveX, moveY) * moveSpeed * Time.unscaledDeltaTime;

        // 刚刚停下移动，记录当前帧索引
        if (moveX == 0 && moveY == 0 && moveZ == 0 && !mouseMoved && (_lastMoveX != 0 || _lastMoveY != 0 || _lastRotateZ != 0))
		{
			_stopMovingServerFrameIndex = ServerFrameIndex;
		}
		else if (moveX != 0 || moveY != 0 || moveZ != 0 || mouseMoved)
		{
			_stopMovingServerFrameIndex = int.MaxValue;
		}
		
        _lastMoveX = moveX;
        _lastMoveY = moveY;
        _lastRotateZ = moveZ;
        _lastMousePos = Input.mousePosition;

        foreach (var commandHandler in commandHandlers)
        {
            CommandInput cmd = new CommandInput();
            if (commandHandler.CheckInput(ref cmd))
            {
                _commands.Add(cmd);
            }
        }

        if (_processingSnapshot != null)
            InterpolateTo(_processingSnapshot, Time.unscaledDeltaTime);
        else if (_lastProcessedSnapshot != null)
            ExtrapolateFrom(_lastProcessedSnapshot);
    }

    private Snapshot _processingSnapshot;

    private void RemoveOutdatedSnapshots()
    {
        List<int> outdatedFrameIndicies = new List<int>();
        foreach (var frameIndex in _snapshots.Keys)
        {
            if (frameIndex < ClientFrameIndex - 1)
                outdatedFrameIndicies.Add(frameIndex);
        }
        foreach (var frameIndex in outdatedFrameIndicies)
        {
            _snapshots.Remove(frameIndex);
        }
    }

    private Snapshot FindNextSnapshot()
    {
        Snapshot snapshot = null;
        int startFramIndex = ClientFrameIndex + 1;
        /* UNDONE: 如果强制每次都取下一个快照，会最终导致取的速度大于服务器下发的速度
        if (_lastProcessedSnapshot != null)
            startFramIndex = Mathf.Max(startFramIndex, _lastProcessedSnapshot.frameIndex + 1);
        */
        for (int i = startFramIndex; i <= _maxReceivedFrame; i++)
        {
            if (_snapshots.TryGetValue(i, out snapshot))
            {
                return snapshot;
            }
        }
        int[] keys = new int[_snapshots.Count];
        _snapshots.Keys.CopyTo(keys, 0);
        DebugReturn(DebugLevel.WARNING, string.Format("Can't find any snapshot between {0} and {1}, last: {2}", startFramIndex, ServerFrameIndex, keys[keys.Length - 1]));
        return null;
    }

    private void InterpolateTo(Snapshot snapshot, float lerpDuration = 0)
    {
        //Debug.LogFormat("{0}(actorNr={1}) interpolating to snapshot {2}...", name, _myActorNr, snapshot.frameIndex);
        long currentFrameTime = (_lastProcessedSnapshot == null ? ClientTime : _lastProcessedSnapshot.frameTime);
        if (lerpDuration == 0)
        {
            lerpDuration = Mathf.Clamp01((snapshot.frameTime - currentFrameTime) / 1000f);
        }

        // 插值同步物件的位置
        var existingObjIds = new HashSet<int>();
        foreach (var objState in snapshot.objectStates)
        {
            ClientObject obj;
            if (!_objects.TryGetValue(objState.id, out obj))
            {
                /*
                DebugReturn(DebugLevel.OFF, string.Format("Bullet doesn't exist, trying to create from snapshot, objId={0}", objState.id));

                // 如果同步的物件本地不存在，则尝试创建
                var commandInput = snapshot.myPlayerState.commands.FirstOrDefault(cmd => cmd.objIds.Contains(objState.id));
                if (commandInput.type == 0)
                {
                    foreach (var otherPlayer in snapshot.otherPlayerStates)
                    {
                        // FIXME: 动画事件创建的物件，所在帧的指令可能为空。应该按角色身上的objIds查找
                        commandInput = otherPlayer.commands.FirstOrDefault(cmd => cmd.objIds.Contains(objState.id));
                        if (commandInput.type != 0)
                        {
                            obj = CreateBullet(objState.id, _otherPlayers[otherPlayer.actorNr], commandInput);
                            break;
                        }
                    }
                }
                else
                {
                    obj = CreateBullet(objState.id, MyPlayer, commandInput);
                }
                */
            }

            if (obj != null)
            {
                obj.state = objState;
                // 优化：不插值本地创建的子弹，因为它是预移动的
                if (obj.source != MyPlayer)
                    obj.LerpTo(lerpDuration);
                existingObjIds.Add(objState.id);
            }

        }
        // 如果物件存在于本地，而不在快照里，则删除（clientAuthority = false）
        foreach (var pair in _objects)
        {
            if (pair.Value != null && /*!pair.Value.clientAuthority*/Time.unscaledTime - pair.Value.ClientCreationTime > 1f && !existingObjIds.Contains(pair.Key))
                Destroy(pair.Value.gameObject);
        }

        MyPlayer.state = snapshot.myPlayerState;
        // 如果玩家位置由服务端控制
        if (!MyPlayer.clientAuthority)
        {
            if (snapshot.frameIndex > _stopMovingServerFrameIndex)
            {
                MyPlayer.LerpTo(lerpDuration, snapshot.myPlayerState.moveSpeed);
                //DebugReturn(DebugLevel.OFF, "Set look dir to: " + MyPlayer.transform.forward);
            }
        }

        foreach (var state in snapshot.otherPlayerStates)
        {
            Player player;
            if (!_otherPlayers.TryGetValue(state.actorNr, out player))
                continue;
            //var move = state.moves.LastOrDefault();
            //if (move.x == 0 && move.y == 0)
            //    player.transform.position = Vector3.Lerp(player.transform.position, new Vector3(state.x, state.y), lerpDuration);
            //else
            //    player.transform.position += new Vector3(move.x, move.y) * state.moveSpeed * FrameDeltaTime;

            player.state = state;
            // 插值同步其他玩家的位置
            player.LerpTo(lerpDuration);//, state.moveSpeed);
        }

        // 同步冷却时间
        foreach (var pair in (MyPlayer.state as PlayerState).commandCooldowns)
        {
            var handler = commandHandlers.First(h => h.type == pair.Key);
            if (handler == null)
            {
                DebugReturn(DebugLevel.ERROR, "Can't find handler by type: " + pair.Key);
                continue;
            }
            //handler.cooldown = pair.Value;
            handler.serverCooldown = pair.Value;
        }
    }

    private void ExtrapolateFrom(Snapshot snapshot)
    {
        MyPlayer.state = snapshot.myPlayerState;
        // 不对当前玩家进行外插值
        // 不对物件进行外插值（子弹等物件会有自己的运动逻辑）
        // 对其他玩家进行外插值
        foreach (var pair in _otherPlayers)
        {
            var playerState = snapshot.otherPlayerStates.Find(state => state.actorNr == pair.Key);
            pair.Value.state = playerState;
            if (playerState != null)// && playerState.moves.Length > 0)
            {
                var lastMove = playerState.commands.LastOrDefault(cmd => cmd.type == CommandType.Movement);

                // 按照最新的移动方向外插值
                pair.Value.transform.position += (Vector3)lastMove.moveDir * playerState.moveSpeed * FrameDeltaTime;
            }
        }
    }

    // 根据快照创建（非本地创建的）子弹物件
    private Bullet CreateBullet(int objId, Player source, CommandInput cmd, CommandHandler handler = null)
    {
        DebugReturn(DebugLevel.OFF, string.Format("Create bullet from snapshot, objId={0}, sourcePlayer={1}, cmdType={2}",
            objId, source.playerState.actorNr, cmd.type));

        if (_objects.ContainsKey(objId))
            return null;

        if (handler == null)
            handler = commandHandlers.First(h => h.type == cmd.type);

        // 创建子弹对象
        var bullet = handler.CreateBullet(source);
        //bullet.moveDir = cmd.lookDir;
        bullet.transform.forward = cmd.lookDir;
        bullet.id = objId;
        RegisterObject(bullet);
        return bullet;
    }

    public void DebugReturn(DebugLevel level, string message)
    {
        if (logText != null)
        {
            if (level != DebugLevel.OFF)
                logText.text += string.Format("[{0}] {1}\n", level, message);

            if (level == DebugLevel.WARNING)
                Debug.LogWarning(message);
            else if (level == DebugLevel.ERROR)
                Debug.LogError(message);
            else
                Debug.Log(message);
        }
    }

    public void OnEvent(EventData eventData)
    {
        DebugReturn(DebugLevel.INFO, eventData.ToStringFull());

        if (eventData.Code == (byte)EventCode.Join)
        {
            var actors = (int[])eventData.Parameters[(byte)ParameterKey.Actors];
            foreach (var actorNr in actors)
            {
                if (actorNr == _myActorNr || _otherPlayers.ContainsKey(actorNr))
                    continue;
                var player = Util.Instantiate(playerPrefab, transform);
                player.name = "Player" + actorNr;
                player.GetComponent<Renderer>().material.color = playerColors[actorNr];
                player.gameObject.layer = LayerMask.NameToLayer(transform.name);
                player.gameObject.SetActive(true);
                player.SetClient(this);
                _otherPlayers[actorNr] = player;
            }
        }
        else if (eventData.Code == (byte)EventCode.Leave)
        {
            var actors = (int[])eventData.Parameters[(byte)ParameterKey.Actors];
            foreach (var actorNr in actors)
            {
                Player player;
                if (_otherPlayers.TryGetValue(actorNr, out player))
                {
                    _otherPlayers.Remove(actorNr);
                    Destroy(player);
                }
            }
        }
    }

    public void OnOperationResponse(OperationResponse response)
    {
        //DebugReturn(DebugLevel.INFO, response.ToStringFull());

        if (response.OperationCode == MessageType.SC_TIME)
        {
            //_serverTimeOffset = (long)response.Parameters[0];
            //_frameWatch.Start();
        }
        else if (response.OperationCode == MessageType.SC_SETTINGS)
        {
            frameIntervalInMs = (int)response.Parameters[0];
            Time.fixedDeltaTime = frameIntervalInMs / 1000f;
            _random = new System.Random((int)response.Parameters[1]);
            optimizedMoveCommands = (bool)response.Parameters[2];
        }
        else if (response.OperationCode == MessageType.SC_SNAPSHOT)
        {
            int frameIndex = (int)response.Parameters[255];
            if (frameIndex <= _maxReceivedFrame)
            {
                DebugReturn(DebugLevel.INFO, "Received outdated snapshot, frame: " + frameIndex);
                return;
            }

            var snapshots = Util.CastListParameter<Snapshot>(response.Parameters[0]);
            /*
            var snapshot = new Snapshot()
            {
                frameIndex = frameIndex,
                frameTime = (long)response.Parameters[254],
                myPlayerState = (PlayerState)response.Parameters[0],
                otherPlayerStates = Util.CastListParameter<SharedPlayerState>(response.Parameters[1]),
                objectStates = Util.CastListParameter<ObjectState>(response.Parameters[2]),
                //otherPlayerCommands = (Dictionary<int, CommandInput[]>)response.Parameters[3]
            };
            _snapshots[frameIndex] = snapshot;
            */

            foreach (var snapshot in snapshots)
            {
                _snapshots[snapshot.frameIndex] = snapshot;

                // 第一次收到快照时，直接设置其它玩家的位置
                if (_maxReceivedFrame < 0)
                {
                    MyPlayer.transform.position = snapshot.myPlayerState.position;
                    MyPlayer.transform.forward = snapshot.myPlayerState.lookDir;

                    foreach (var state in snapshot.otherPlayerStates)
                    {
                        _otherPlayers[state.actorNr].transform.position = state.position;
                        _otherPlayers[state.actorNr].transform.forward = state.lookDir;
                    }
                    // TODO: 设置物件的位置？
                }

                //DebugReturn(DebugLevel.OFF, string.Format("Received snapshot {0}, other player skill commands: {1} / {2}",
                //    snapshot.frameIndex, snapshot.otherPlayerStates[0].commands.Count(cmd => cmd.type != CommandType.Movement), snapshot.otherPlayerStates[0].commands.Length));
            }

            _maxReceivedFrame = frameIndex;

            _receivedSnapshotsCount++;
            if (_startFrameIndex == 0)
                _startFrameIndex = frameIndex;
        }
        else if (response.OperationCode == (byte)OperationCode.Join)
        {
            DebugReturn(DebugLevel.INFO, "Joined Room, ActorNr: " + response.Parameters[(byte)ParameterKey.ActorNr]);
            _myActorNr = (int)response.Parameters[(byte)ParameterKey.ActorNr];
            MyPlayer = Util.Instantiate(playerPrefab, transform);
            MyPlayer.name = "MyPlayer" + _myActorNr;
            MyPlayer.GetComponent<Renderer>().material.color = playerColors[_myActorNr];
            MyPlayer.gameObject.layer = LayerMask.NameToLayer(transform.name);
            MyPlayer.SetClient(this);
            MyPlayer.gameObject.SetActive(true);

            _joinServerTime = _peer.ServerTimeInMilliSeconds;
        }

    }

    public void OnStatusChanged(StatusCode statusCode)
    {
        if (statusCode == StatusCode.Connect)
        {
            _peer.OpCustom((byte)OperationCode.Join, new Dictionary<byte, object>() {
                {(byte)ParameterKey.GameId, roomName }}, true);
        }
        else if (statusCode == StatusCode.Disconnect)
        {
            //_serverTimeOffset = 0;
            //_frameWatch.Stop();
            _snapshots.Clear();
        }
    }

    void OnDestroy()
    {
        OnApplicationQuit();
    }

    void OnApplicationQuit()
    {
        if (_peer.PeerState == PeerStateValue.Connected)
        {
            _peer.Disconnect();
        }
    }


 #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (!Application.isPlaying)
            return;

        if (drawFramesAhead && _processingSnapshot != null)
        {
            for (int i = _processingSnapshot.frameIndex; i <= _maxReceivedFrame; i++)
            {
                Snapshot snapshot;
                if (!_snapshots.TryGetValue(i, out snapshot))
                    continue;
                foreach (var otherPlayer in snapshot.otherPlayerStates)
                {
                    Gizmos.color = playerColors[otherPlayer.actorNr] * (1f - 0.5f * (i - _processingSnapshot.frameIndex) / (_maxReceivedFrame - _processingSnapshot.frameIndex));
                    Gizmos.DrawWireCube(otherPlayer.position, Vector3.one * 0.5f);
                }
            }

            /* 测试：绘制鼠标位置
            var mousePos = clientCamera.ScreenToWorldPoint(new Vector3(_lastMousePos.x, _lastMousePos.y, clientCamera.WorldToScreenPoint(MyPlayer.transform.position).z));
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(mousePos, 0.2f);
            */
        }
    }

    /*
 [UnityEditor.CustomEditor(typeof(DemoClient))]
 public class DemoClientEditor : UnityEditor.Editor
 {
     public override void OnInspectorGUI()
     {
         base.OnInspectorGUI();
         var instance = target as DemoClient;
         GUILayout.Label(instance.GetSnapshotDebugString());
     }
 }
     */

#endif
}
