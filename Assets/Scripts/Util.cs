﻿using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static T Instantiate<T>(T comp, Transform parent) where T : Component
    {
#if UNITY_5_3_OR_NEWER
        return Object.Instantiate(comp, parent);
#else
        var obj = (Object.Instantiate(comp.gameObject) as GameObject).GetComponent<T>();
        obj.transform.SetParent(parent);
        return obj;
#endif
    }

    public static List<T> CastListParameter<T>(object parameter)
    {
        List<T> result;
        if (parameter is System.Array && (parameter as System.Array).Length > 0)
        {
            result = new List<T>();
            foreach (T element in (System.Array)parameter)
                result.Add(element);
        }
        else if (parameter is List<T>)
            result = (List<T>)parameter;
        else
            result = new List<T>();
        return result;
    }

}
