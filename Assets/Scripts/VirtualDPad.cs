﻿using UnityEngine;
using UnityEngine.EventSystems;

public class VirtualDPad : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform knob;
    public Vector2 Direction { get; private set; }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 localPos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(transform as RectTransform, eventData.position, eventData.pressEventCamera, out localPos))
        {
            Direction = localPos.normalized;
            var radius = (transform as RectTransform).rect.width * 0.5f;
            //if (localPos.magnitude > radius)
                knob.anchoredPosition = radius * Direction;
            //else
            //    knob.anchoredPosition = localPos;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Direction = Vector2.zero;
        knob.anchoredPosition = Vector2.zero;
    }
}
