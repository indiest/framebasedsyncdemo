﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NetworkSimulationOverlay : MonoBehaviour
{
    public DemoClient demoClient1;
    public DemoClient demoClient2;
    public Toggle toggleNS1;
    public Text labelNSLag1;
    public Slider sliderNSLag1;
    public Text labelNSLoss1;
    public Slider sliderNSLoss1;
    public Toggle toggleNS2;
    public Text labelNSLag2;
    public Slider sliderNSLag2;
    public Text labelNSLoss2;
    public Slider sliderNSLoss2;
    public Button buttonClose;

    void Awake()
    {
        toggleNS1.isOn = demoClient1.Peer.IsSimulationEnabled;
        toggleNS2.isOn = demoClient2.Peer.IsSimulationEnabled;
        sliderNSLag1.value = demoClient1.Peer.NetworkSimulationSettings.IncomingLag;
        sliderNSLag2.value = demoClient2.Peer.NetworkSimulationSettings.IncomingLag;
        sliderNSLoss1.value = demoClient1.Peer.NetworkSimulationSettings.IncomingLossPercentage;
        sliderNSLoss2.value = demoClient2.Peer.NetworkSimulationSettings.IncomingLossPercentage;

        toggleNS1.onValueChanged.AddListener(OnNS1Changed);
        toggleNS2.onValueChanged.AddListener(OnNS2Changed);
        sliderNSLag1.onValueChanged.AddListener(OnLag1Changed);
        sliderNSLag2.onValueChanged.AddListener(OnLag2Changed);
        sliderNSLoss1.onValueChanged.AddListener(OnLoss1Changed);
        sliderNSLoss2.onValueChanged.AddListener(OnLoss2Changed);
        buttonClose.onClick.AddListener(ToggleOverlay);
    }

    public void ToggleOverlay()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }


    private void OnNS1Changed(bool value)
    {
        demoClient1.Peer.IsSimulationEnabled = value;
    }

    private void OnNS2Changed(bool value)
    {
        demoClient2.Peer.IsSimulationEnabled = value;
    }

    private void OnLag1Changed(float value)
    {
        labelNSLag1.text = string.Format("Lag: {0}ms", value);
        var settings = demoClient1.Peer.NetworkSimulationSettings;
        settings.IncomingLag = settings.OutgoingLag = (int)value;
    }

    private void OnLag2Changed(float value)
    {
        labelNSLag2.text = string.Format("Lag: {0}ms", value);
        var settings = demoClient2.Peer.NetworkSimulationSettings;
        settings.IncomingLag = settings.OutgoingLag = (int)value;
    }

    private void OnLoss1Changed(float value)
    {
        labelNSLoss1.text = string.Format("Loss: {0}%", value);
        var settings = demoClient1.Peer.NetworkSimulationSettings;
        settings.IncomingLossPercentage = settings.OutgoingLossPercentage = (int)value;
    }

    private void OnLoss2Changed(float value)
    {
        labelNSLoss2.text = string.Format("Loss: {0}%", value);
        var settings = demoClient2.Peer.NetworkSimulationSettings;
        settings.IncomingLossPercentage = settings.OutgoingLossPercentage = (int)value;
    }

}
