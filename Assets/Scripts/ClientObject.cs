﻿using UnityEngine;
using System.Collections;
using FrameBasedSyncFramework;

public class ClientObject : MonoBehaviour
{
    public int id;
    [System.NonSerialized]
    public ObjectState state;
    [System.NonSerialized]
    public Player source;
    public float ClientCreationTime { get; private set; }

    void Awake()
    {
        ClientCreationTime = Time.unscaledTime;
    }

    // 是否由客户端控制（创建、销毁等）
    public bool clientAuthority;

    private Vector3 _lerpVelocity;

    public virtual void LerpTo(float duration, float maxSpeed = float.MaxValue)
    {
        if (state == null)
            return;

        transform.position = Vector3.SmoothDamp(transform.position, state.position, ref _lerpVelocity, duration, maxSpeed);
    }
}
